# Atmospheric Science Tools
Implementation of various tools to analyse, transform and plot data.
Additional functions are available to calculate various parameters,
i.e. density of dry air or surface tension of sea water.
There are also some functions to calculate the transport efficiency
for a given experimental setup.

# Installation
Clone this repository by running the following inside a command-line interface (CLI).

```
git clone https://codebase.helmholtz.cloud/alexander.boehmlaender/as_tools.git
```

# Create a custom environment
Work in progress currently.
