# -*- coding: utf-8 -*-
"""
Created on Tue Jun  1 12:01:23 2021

@author: uudyv
A list of functions that can be used to ease the construction of plots, to analysis some data, etc.
"""
import pandas as pd
import matplotlib.pyplot as plt


def heatmap(data, row_labels, col_labels, ax=None,
            cbar_kw={}, cbarlabel="", **kwargs):
    """
    Create a heatmap from a numpy array and two lists of labels.

    Parameters
    ----------
    data
        A 2D numpy array of shape (N, M).
    row_labels
        A list or array of length N with the labels for the rows.
    col_labels
        A list or array of length M with the labels for the columns.
    ax
        A `matplotlib.axes.Axes` instance to which the heatmap is plotted.  If
        not provided, use current axes or create a new one.  Optional.
    cbar_kw
        A dictionary with arguments to `matplotlib.Figure.colorbar`.  Optional.
    cbarlabel
        The label for the colorbar.  Optional.
    **kwargs
        All other arguments are forwarded to `imshow`.
    """

    import matplotlib.pyplot as plt
    import numpy as np

    if not ax:
        ax = plt.gca()

    # Plot the heatmap
    im = ax.imshow(data, **kwargs)

    # We want to show all ticks...
    ax.set_xticks(np.arange(data.shape[1]))
    ax.set_yticks(np.arange(data.shape[0]))
    # ... and label them with the respective list entries.
    ax.set_xticklabels(col_labels)
    ax.set_yticklabels(row_labels)

    # Let the horizontal axes labeling appear on top.
    ax.tick_params(top=True, bottom=False,
                   labeltop=True, labelbottom=False)

    # Rotate the tick labels and set their alignment.
    plt.setp(ax.get_xticklabels(), rotation=-45, ha="right",
             rotation_mode="anchor")

    ax.set_xticks(np.arange(data.shape[1]+1)-.5, minor=True)
    ax.set_yticks(np.arange(data.shape[0]+1)-.5, minor=True)
    ax.grid(False, which='both')
    ax.tick_params(which="minor", bottom=False, left=False)

    # correct spacing
    im_ratio = data.shape[0]/data.shape[1]

    # Create colorbar
    cbar = ax.figure.colorbar(im, ax=ax, fraction=0.046*im_ratio, pad=0.04, **cbar_kw)
    cbar.ax.set_ylabel(cbarlabel, rotation=-90, va="bottom")

    return im, cbar


def annotate_heatmap(im, data=None, valfmt="{x:.2f}",
                     textcolors=("black", "white"),
                     threshold=None, **textkw):
    """
    A function to annotate a heatmap.

    Parameters
    ----------
    im
        The AxesImage to be labeled.
    data
        Data used to annotate.  If None, the image's data is used.  Optional.
    valfmt
        The format of the annotations inside the heatmap.  This should either
        use the string format method, e.g. "$ {x:.2f}", or be a
        `matplotlib.ticker.Formatter`.  Optional.
    textcolors
        A pair of colors.  The first is used for values below a threshold,
        the second for those above.  Optional.
    threshold
        Value in data units according to which the colors from textcolors are
        applied.  If None (the default) uses the middle of the colormap as
        separation.  Optional.
    **kwargs
        All other arguments are forwarded to each call to `text` used to create
        the text labels.
    """

    import matplotlib
    import numpy as np

    if not isinstance(data, (list, np.ndarray)):
        data = im.get_array()

    # Normalize the threshold to the images color range.
    if threshold is not None:
        threshold = im.norm(threshold)
    else:
        threshold = im.norm(data.max())/2.

    # Set default alignment to center, but allow it to be
    # overwritten by textkw.
    kw = dict(horizontalalignment="center",
              verticalalignment="center")
    kw.update(textkw)

    # Get the formatter in case a string is supplied
    if isinstance(valfmt, str):
        valfmt = matplotlib.ticker.StrMethodFormatter(valfmt)

    # Loop over the data and create a `Text` for each "pixel".
    # Change the text's color depending on the data.
    texts = []
    for i in range(data.shape[0]):
        for j in range(data.shape[1]):
            kw.update(color=textcolors[int(im.norm(data[i, j]) < threshold)])
            text = im.axes.text(j, i, valfmt(data[i, j], None), **kw)
            texts.append(text)

    return texts


def sd_timeseries(sd_df: pd.DataFrame, data_min: float | None, data_max: float | None,
                  times: iter = None, bin_edges: iter = None,
                  cbar_label: str = r'd$N$/dlog$d_\mathrm{p}$ / cm$^{-3}$',
                  colormap: str = 'viridis') -> tuple[plt.Figure, plt.Axes]:
    r"""
    Function to plot size distribution data.

    Parameters
    ----------
    sd_df : pd.DataFrame
        DataFrame needs to contain times on index and diameter on column.
    data_min : float | None
        Smallest value that is not zero. Useful to calculate the smallest exponent.
    data_max : float | None
        Highest value. Useful to calculate the highest exponent (+1).
    times : iter, optional
        Edges of temporal grid. The default is None.
    bin_edges : iter, optional
        Edges of size distribution bins. The default is None.
    cbar_label : str, optional
        Label of colorbar. The default is r'd$N$/dlog$d_\mathrm{p}$ / cm$^{-3}$'.
    colormap : str, optional
        Colormap name as specified in matplotlib. The default is 'viridis'.

    Returns
    -------
    fig : plt.Figure
        Figure object that can be saved or shown, i.e. fig.savefig() or fig.show().
    ax : plt.Axes
        Axes object that can be accesed, i.e. to add a title with ax.set_title().

    """
    from omccolors import omccolors
    import matplotlib.pyplot as plt
    import matplotlib.colors as mcolors
    import matplotlib.dates as mdates

    (colormap, min_exp, max_exp) = omccolors.generate_omc(data_min,
                                                          data_max,
                                                          colormap)

    if isinstance(times, type(None)):
        times = sd_df.index
    if isinstance(bin_edges, type(None)):
        bin_edges = sd_df.columns
    fig, ax = plt.subplots()
    c = ax.pcolormesh(times, bin_edges,
                      sd_df.T,
                      norm=mcolors.LogNorm(vmin=10**min_exp, vmax=10**max_exp),
                      cmap=colormap)
    fig.colorbar(c, ax=ax, label=cbar_label)
    locator = mdates.AutoDateLocator()
    formatter = mdates.ConciseDateFormatter(locator, tz='UTC', show_offset=False)
    ax.get_xaxis().set_major_locator(locator)
    ax.get_xaxis().set_major_formatter(formatter)
    ax.set_ylabel(r'$d_\mathrm{p}$ / µm')
    ax.set_xlabel(f'{times[0].tzinfo}')
    ax.set_yscale('log')

    return fig, ax
