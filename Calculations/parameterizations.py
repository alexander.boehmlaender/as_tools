# -*- coding: utf-8 -*-
"""
Created on Fri Mar  4 14:23:49 2022

@author: st5536
Collection of parameterizations for INP concentrations and INAS densities.

"""

import numpy as np


def inas_parameterizations(T, material):
    match material:
        case 'dust':
            a = 1e-4
            b0 = 8.934
            b1 = -0.517
            return a * np.exp(b1 * T + b0)
        case 'illite':
            b0 = 6.53043e4
            b1 = -8.215309e2
            b2 = 3.446885376
            b3 = -4.822268e-3
            return np.exp(b3 * (T + 273.15)**3 + b2 * (T + 273.15)**2 + b1 * (T + 273.15)**1 + b0)
        case 'soot':  # Ullrich 2017, soot
            a = 7.463
            b0 = 0.7667
            b1 = -0.8525
            b2 = -0.0101
            return 1e-4 * a * np.exp(b2 * T**2 + b1 * T + b0)
        case 'volcanic ash':
            b0 = -22.05302
            b1 = -1.87203
            b2 = -0.02287
            return np.exp(b2 * T**2 + b1 * T**1 + b0)
        case 'birch pollen':
            b0 = -297.26
            b1 = -42.938
            b2 = -1.9893
            b3 = -0.0309
            return np.exp(b3 * T**3 + b2 * T**2 + b1 * T**1 + b0)
        case 'cladosporidium spores':
            b0 = -1056.63326
            b1 = -96.59842
            b2 = -2.91415
            b3 = -0.02934
            return np.exp(b3 * T**3 + b2 * T**2 + b1 * T**1 + b0)
        case 'psyringae':
            b0 = -10.399
            b1 = -5.1067
            b2 = -0.4325
            return np.exp(b2 * T**2 + b1 * T**1 + b0)
        case 'boreal forest summertime':
            b0 = 154
            b1 = -0.543
            return 1e-4 * np.exp(b1 * (T + 273.15)**1 + b0)
        case 'boreal forest transition period':
            b0 = 140
            b1 = -0.49
            return 1e-4 * np.exp(b1 * (T + 273.15)**1 + b0)
        case 'boreal forest wintertime':
            b0 = 141
            b1 = -0.495
            return 1e-4 * np.exp(b1 * (T + 273.15)**1 + b0)
