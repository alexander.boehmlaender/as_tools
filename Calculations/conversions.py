# -*- coding: utf-8 -*-
"""
Created on Fri May 27 11:55:17 2022

@author: st5536
Collection of simple conversion functions.
"""

import numpy as np

from .formulas import cunningham_slip_correction


def C2K(x: float) -> float:
    """Transform celsius to kelvin."""
    return x + 273.15


def K2C(x: float) -> float:
    """Transform kelvin to celsius."""
    return x - 273.15


def height_to_pressure(h: float) -> float:
    """
    Calculate atmospheric pressure at a specific height above sea level.

    Parameters
    ----------
    h : float
        Height above sea level in m.

    Returns
    -------
    float
        Atmospheric pressure in hPa.

    """
    p_0 = 1013.25  # hPa
    L = 0.00976  # K / m
    T_0 = 288.16  # K
    c_p = 1004.68506  # J / (kg K)
    M = 0.02896968  # m / s^2
    R_0 = 8.314462618  # J / (mol K)
    p = p_0 * (1 - L * h / T_0)**(c_p * M / R_0)
    return p


def pressure_to_height(p: float) -> float:
    """
    Calculate height above sea level from atmospheric pressure.

    Parameters
    ----------
    p : float
        Atmospheric pressure in hPa.

    Returns
    -------
    float
        Height above sea level in m.

    """
    p_0 = 1013.25  # hPa
    L = 0.00976  # K / m
    T_0 = 288.16  # K
    c_p = 1004.68506  # J / (kg K)
    M = 0.02896968  # m / s^2
    R_0 = 8.314462618  # J / (mol K)
    h = T_0 / L * (1 - (p / p_0)**(R_0 / (c_p * M)))
    return h


def stokes_diameter_from_volume_equivalent_diameter(volume_equivalent_diameter: float,
                                                    ambient_pressure: float,
                                                    ambient_temperature: float,
                                                    shape_factor: float,
                                                    iterations: int = 5) -> float:
    """
    Calculate Stokes diameter from volume equivalent diameter.
    The Stokes diameter is the diameter of a spherical particle
    with the same density and gravitational settling velocity
    as the particle in question (Kulkarni2011, p. 25).
    Calculation is done iteratively using equation 9.107
    from Seinfeld2006.

    Parameters
    ----------
    volume_equivalent_diameter : float
        Volume equivalent diameter in m.
    ambient_pressure : float
        Ambient pressure in Pa.
    ambient_temperature : float
        Ambient temperature in K.
    shape_factor : float
        Shape factor (dimensionless).
    iterations : int, optional
        Number of iterations for iterative process.
        Diameter converges very fast (> 3). The default is 5.

    Returns
    -------
    float
        Stokes diameter in m.

    """
    index = iterations - 1
    while index >= 1:
        if index == iterations - 1:
            stokes_diameter = (volume_equivalent_diameter
                               * np.sqrt(cunningham_slip_correction(volume_equivalent_diameter,
                                                                    ambient_pressure,
                                                                    ambient_temperature)
                                         / (cunningham_slip_correction(volume_equivalent_diameter,
                                                                       ambient_pressure,
                                                                       ambient_temperature)
                                            * shape_factor)
                                         )
                               )
        else:
            stokes_diameter = (volume_equivalent_diameter
                               * np.sqrt(cunningham_slip_correction(volume_equivalent_diameter,
                                                                    ambient_pressure,
                                                                    ambient_temperature)
                                         / (cunningham_slip_correction(stokes_diameter,
                                                                       ambient_pressure,
                                                                       ambient_temperature)
                                            * shape_factor)
                                         )
                               )
        index -= 1
    return stokes_diameter


def aerodynamic_diameter_from_volume_equivalent_diameter(volume_equivalent_diameter: float,
                                                         ambient_pressure: float,
                                                         ambient_temperature: float,
                                                         particle_density: float,
                                                         shape_factor: float,
                                                         iterations: int = 5) -> float:
    """
    Calculate aerodynamic diameter from volume equivalent diameter.
    The aerodynamic diameter is the diameter of a standard density
    particle with the same terminal velocity as the particle in
    question (Seinfeld2006, p. 429).
    Calculation is done iteratively using equation 9.110
    from Seinfeld2006.

    Parameters
    ----------
    volume_equivalent_diameter : float
        Volume equivalent diameter in m.
    ambient_pressure : float
        Ambient pressure in Pa.
    ambient_temperature : float
        Ambient temperature in K.
    particle_density : float
        Particle density in kg / m3.
    shape_factor : float
        Shape factor (dimensionless).
    iterations : int, optional
        Number of iterations for iterative process.
        Diameter converges very fast (> 3). The default is 5.

    Returns
    -------
    float
        Aerodynamic particle diameter in m.

    """
    particle_standard_density = 1000  # kg / m3
    index = iterations - 1
    while index >= 1:
        if index == iterations - 1:
            aero_diameter = (volume_equivalent_diameter * np.sqrt(particle_density
                                                                  / particle_standard_density)
                             * np.sqrt(cunningham_slip_correction(volume_equivalent_diameter,
                                                                  ambient_pressure,
                                                                  ambient_temperature)
                                       / (cunningham_slip_correction(volume_equivalent_diameter,
                                                                     ambient_pressure,
                                                                     ambient_temperature)
                                          * shape_factor)
                                       )
                             )
        else:
            aero_diameter = (volume_equivalent_diameter * np.sqrt(particle_density
                                                                  / particle_standard_density)
                             * np.sqrt(cunningham_slip_correction(volume_equivalent_diameter,
                                                                  ambient_pressure,
                                                                  ambient_temperature)
                                       / (cunningham_slip_correction(aero_diameter,
                                                                     ambient_pressure,
                                                                     ambient_temperature)
                                          * shape_factor)
                                       )
                             )
        index -= 1
    return aero_diameter


def volume_equivalent_diameter_from_aerodynamic_diameter(aerodynamic_diameter: float,
                                                         ambient_pressure: float,
                                                         ambient_temperature: float,
                                                         particle_density: float,
                                                         iterations: int = 5) -> float:
    """
    Calculate aerodynamic diameter from volume equivalent diameter.
    The aerodynamic diameter is the diameter of a standard density
    particle with the same terminal velocity as the particle in
    question (Seinfeld2006, p. 429).
    Calculation is done iteratively using equation 9.110
    from Seinfeld2006.

    Parameters
    ----------
    volume_equivalent_diameter : float
        Volume equivalent diameter in m.
    ambient_pressure : float
        Ambient pressure in Pa.
    ambient_temperature : float
        Ambient temperature in K.
    particle_density : float
        Particle density in kg / m3.
    iterations : int, optional
        Number of iterations for iterative process.
        Diameter converges very fast (> 3). The default is 5.

    Returns
    -------
    float
        Aerodynamic particle diameter in m.

    """
    particle_standard_density = 1000  # kg / m3
    index = iterations - 1
    while index >= 1:
        if index == iterations - 1:
            volume_equivalent_diameter = (aerodynamic_diameter
                                          * np.sqrt(particle_density
                                                    / particle_standard_density)**(-1)
                                          * np.sqrt(cunningham_slip_correction(
                                              aerodynamic_diameter,
                                              ambient_pressure,
                                              ambient_temperature)
                                                    / (cunningham_slip_correction(
                                                        aerodynamic_diameter,
                                                        ambient_pressure,
                                                        ambient_temperature)
                                                       )
                                                    )
                                          )
        else:
            volume_equivalent_diameter = (aerodynamic_diameter
                                          * np.sqrt(particle_density
                                                    / particle_standard_density)**(-1)
                                          * np.sqrt(cunningham_slip_correction(
                                              aerodynamic_diameter,
                                              ambient_pressure,
                                              ambient_temperature)
                                                    / (cunningham_slip_correction(
                                                        volume_equivalent_diameter,
                                                        ambient_pressure,
                                                        ambient_temperature)
                                                       )
                                                    )
                                          )
        index -= 1
    return volume_equivalent_diameter


def electrical_mobility_diameter_from_volume_equivalent_diameter(volume_equivalent_diameter: float,
                                                                 ambient_pressure: float,
                                                                 ambient_temperature: float,
                                                                 shape_factor: float,
                                                                 iterations: int = 5) -> float:
    """
    Calculate electrical mobility diameter from volume equivalent diameter.
    The electrical mobility diameter is the diameter of a standard density
    particle with the same electrical mobility as the particle in
    question (Seinfeld2006, p. 431).
    Calculation is done iteratively using equation 9.114
    from Seinfeld2006.

    Parameters
    ----------
    volume_equivalent_diameter : float
        Volume equivalent diameter in m.
    ambient_pressure : float
        Ambient pressure in Pa.
    ambient_temperature : float
        Ambient temperature in K.
    shape_factor : float
        Shape factor (dimensionless).
    iterations : int, optional
        Number of iterations for iterative process.
        Diameter converges very fast (> 3). The default is 5.

    Returns
    -------
    float
        Aerodynamic particle diameter in m.

    """
    index = iterations - 1
    while index >= 1:
        if index == iterations - 1:
            electrical_diameter = (volume_equivalent_diameter * shape_factor
                                   * (cunningham_slip_correction(volume_equivalent_diameter,
                                                                 ambient_pressure,
                                                                 ambient_temperature)
                                      / cunningham_slip_correction(volume_equivalent_diameter,
                                                                   ambient_pressure,
                                                                   ambient_temperature)
                                      )
                                   )
        else:
            electrical_diameter = (volume_equivalent_diameter * shape_factor
                                   * (cunningham_slip_correction(electrical_diameter,
                                                                 ambient_pressure,
                                                                 ambient_temperature)
                                      / cunningham_slip_correction(volume_equivalent_diameter,
                                                                   ambient_pressure,
                                                                   ambient_temperature)
                                      )
                                   )
        index -= 1
    return electrical_diameter


def volume_equivalent_diameter_from_electrical_mobility_diameter(electrical_mobility_diameter: float,
                                                                 ambient_pressure: float,
                                                                 ambient_temperature: float,
                                                                 shape_factor: float,
                                                                 iterations: int = 5) -> float:
    """
    Calculate electrical mobility diameter from volume equivalent diameter.
    The electrical mobility diameter is the diameter of a standard density
    particle with the same electrical mobility as the particle in
    question (Seinfeld2006, p. 431).
    Calculation is done iteratively using equation 9.114
    from Seinfeld2006.

    Parameters
    ----------
    volume_equivalent_diameter : float
        Volume equivalent diameter in m.
    ambient_pressure : float
        Ambient pressure in Pa.
    ambient_temperature : float
        Ambient temperature in K.
    shape_factor : float
        Shape factor (dimensionless).
    iterations : int, optional
        Number of iterations for iterative process.
        Diameter converges very fast (> 3). The default is 5.

    Returns
    -------
    float
        Aerodynamic particle diameter in m.

    """
    index = iterations - 1
    while index >= 1:
        if index == iterations - 1:
            volume_equivalent_diameter = (electrical_mobility_diameter * shape_factor**(-1)
                                          * (cunningham_slip_correction(
                                              electrical_mobility_diameter,
                                              ambient_pressure,
                                              ambient_temperature)
                                             / cunningham_slip_correction(
                                                 electrical_mobility_diameter,
                                                 ambient_pressure,
                                                 ambient_temperature)
                                             )
                                          )
        else:
            volume_equivalent_diameter = (electrical_mobility_diameter * shape_factor
                                          * (cunningham_slip_correction(volume_equivalent_diameter,
                                                                        ambient_pressure,
                                                                        ambient_temperature)
                                             / cunningham_slip_correction(
                                                 electrical_mobility_diameter,
                                                 ambient_pressure,
                                                 ambient_temperature)
                                             )
                                          )
        index -= 1
    return volume_equivalent_diameter


def volume_flow_to_std_flow(volumetric_flow: float,
                            ambient_temperature: float,
                            ambient_pressure: float,
                            standard_temperature: float = 273.15,
                            standard_pressure: float = 1013.25) -> float:
    """
    Calculates the standard flow from volumetric flow.

    Parameters
    ----------
    volumetric_flow : float
        Volumetric flow in l/min.
    ambient_temperature : float
        Ambient temperature in K.
    ambient_pressure : float
        Ambient pressure in hPa.
    standard_temperature : float, optional
        Standard temperature in K. The default is 273.15.
    standard_pressure : float, optional
        Standard pressure in hPa. The default is 1013.25.

    Returns
    -------
    float
        Standard flow in stdl/min.

    """
    standard_flow = (volumetric_flow
                     * (standard_temperature / ambient_temperature)
                     * (ambient_pressure / standard_pressure))
    return standard_flow


def std_flow_to_volumetric_flow(standard_flow: float,
                                ambient_temperature: float,
                                ambient_pressure: float,
                                standard_temperature: float = 273.15,
                                standard_pressure: float = 1013.25) -> float:
    """
    Calculates the volumetric flow from standard flow.

    Parameters
    ----------
    standard flow : float
        Standard flow in stdl/min.
    ambient_temperature : float
        Ambient temperature in K.
    ambient_pressure : float
        Ambient pressure in hPa.
    standard_temperature : float, optional
        Standard temperature in K. The default is 273.15.
    standard_pressure : float, optional
        Standard pressure in hPa. The default is 1013.25.

    Returns
    -------
    float
        Volumetric flow in l/min.

    """
    volumetric_flow = (standard_flow
                       * (ambient_temperature / standard_temperature)
                       * (standard_pressure / ambient_pressure))
    return volumetric_flow
