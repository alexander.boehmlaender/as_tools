# -*- coding: utf-8 -*-
"""
Created on Fri May 27 11:57:49 2022

@author: st5536
Size distribution functions.
"""

import pandas as pd
import numpy as np
import numpy.typing as npt


def surface_distribution(number_distribution: pd.DataFrame) -> pd.DataFrame:
    """
    Calculate surface distribution from number distribution.
    Number distribution has to have bin diameters as columns.
    Bin diameters should be supplied in µm.
    Definition taken from [1]_.

    Parameters
    ----------
    number_distribution : pd.DataFrame
        Number distribution (particle concentration per size bin).
        Number distribution is given as a concentration.

    Returns
    -------
    pd.DataFrame
        Surface distribution for a given number distribution.
        Unit is µm2 / cm3. Where µm originates from the columns.

    References
    ----------
    [1] Seinfeld, J. H. and S. N. Pandis (2016). Atmospheric chemistry and physics:
    from air pollution to climate change. John Wiley & Sons. isbn: 1118947401.

    """
    factor = np.pi * number_distribution.columns**2
    surface_distribution = number_distribution.mul(factor, axis=1)
    return surface_distribution


def volume_distribution(number_distribution: pd.DataFrame) -> pd.DataFrame:
    """
    Calculate volume distribution from number distribution.
    Number distribution has to have bin diameters as columns.
    Bin diameters should be supplied in µm.
    Definition taken from [1]_.

    Parameters
    ----------
    number_distribution : pd.DataFrame
        Number distribution (particle concentration per size bin).
        Number distribution is given as a concentration.

    Returns
    -------
    pd.DataFrame
        Volume distribution for a given number distribution.
        Unit is µm3 / cm3. Where µm originates from the columns.

    References
    ----------
    [1] Seinfeld, J. H. and S. N. Pandis (2016). Atmospheric chemistry and physics:
    from air pollution to climate change. John Wiley & Sons. isbn: 1118947401.

    """
    factor = np.pi * number_distribution.columns**3 / 6
    volume_distribution = number_distribution.mul(factor, axis=1)
    return volume_distribution


def mass_distribution(number_distribution: pd.DataFrame,
                      particle_density: float = 1.6) -> pd.DataFrame:
    """
    Calculate mass distribution from number distribution
    and a corresponding particle density (in g / cm3).
    Factor 1e-24 results from unit conversion.
    Definition taken from [1]_.

    Parameters
    ----------
    number_distribution : pd.DataFrame
        Number distribution (particle concentration per size bin).
        Number distribution is given as a concentration.
    particle_density : float, optional
        Particle density in g / cm3. The default is 1.6.

    Returns
    -------
    pd.DataFrame
        Mass distribution for a given number distribution
        and particle density.
        Unit is µg / m3.

    References
    ----------
    [1] Seinfeld, J. H. and S. N. Pandis (2016). Atmospheric chemistry and physics:
    from air pollution to climate change. John Wiley & Sons. isbn: 1118947401.

    """
    mass_distribution = volume_distribution(number_distribution) * particle_density
    return mass_distribution


def lognormal_number_distribution(number_concentration: float,
                                  standard_deviation: float,
                                  median_particle_diameter: float,
                                  particle_diameter: npt.ArrayLike) -> npt.ArrayLike:
    """
    Lognormal number distribution with median diameter and
    standard deviation, both on log-scale. Log is related to
    the decadal logarithm. Definition taken from [1]_.

    Parameters
    ----------
    number_concentration : float
        Total number concentration of particles.
    standard_deviation : float
        Standard deviation..
    median_particle_diameter : float
        Median diameter.
    particle_diameter : npt.ArrayLike
        Particle diameter of a given size distribution.

    Returns
    -------
    npt.ArrayLike
        Lognormal number distribution based on the median
        particle diameter and the standard deviation,
        both on the log-scale.

    References
    ----------
    [1] Seinfeld, J. H. and S. N. Pandis (2016). Atmospheric chemistry and physics:
    from air pollution to climate change. John Wiley & Sons. isbn: 1118947401.

    """
    dNddp = (number_concentration
                / (np.log(standard_deviation) * particle_diameter * np.sqrt(2 * np.pi)
                   )
                * np.exp(- (np.log(particle_diameter) - np.log(median_particle_diameter))**2
                         / (2 * np.log(standard_deviation)**2)
                         )
                )
    return dNddp

def lognormal_number_distribution_natural_scale(number_concentration: float,
                                                standard_deviation: float,
                                                median_particle_diameter: float,
                                                particle_diameter: npt.ArrayLike) -> npt.ArrayLike:
    """
    Lognormal number distribution with median diameter and
    standard deviation, both on log-scale. Log is related to
    the decadal logarithm. Definition taken from [1]_.

    Parameters
    ----------
    number_concentration : float
        Total number concentration of particles.
    standard_deviation : float
        Standard deviation..
    median_particle_diameter : float
        Median diameter.
    particle_diameter : npt.ArrayLike
        Particle diameter of a given size distribution.

    Returns
    -------
    npt.ArrayLike
        Lognormal number distribution based on the median
        particle diameter and the standard deviation,
        both on the log-scale.

    References
    ----------
    [1] Seinfeld, J. H. and S. N. Pandis (2016). Atmospheric chemistry and physics:
    from air pollution to climate change. John Wiley & Sons. isbn: 1118947401.

    """
    dNdlndp = lognormal_number_distribution(number_concentration,
                                            standard_deviation,
                                            median_particle_diameter,
                                            particle_diameter) * particle_diameter
    return dNdlndp


def lognormal_number_distribution_log_scale(number_concentration: float,
                                            standard_deviation: float,
                                            median_particle_diameter: float,
                                            particle_diameter: npt.ArrayLike) -> npt.ArrayLike:
    """
    Lognormal number distribution with median diameter and
    standard deviation, both on log-scale. Log is related to
    the decadal logarithm. Definition taken from [1]_.

    Parameters
    ----------
    number_concentration : float
        Total number concentration of particles.
    standard_deviation : float
        Standard deviation..
    median_particle_diameter : float
        Median diameter.
    particle_diameter : npt.ArrayLike
        Particle diameter of a given size distribution.

    Returns
    -------
    npt.ArrayLike
        Lognormal number distribution based on the median
        particle diameter and the standard deviation,
        both on the log-scale.

    References
    ----------
    [1] Seinfeld, J. H. and S. N. Pandis (2016). Atmospheric chemistry and physics:
    from air pollution to climate change. John Wiley & Sons. isbn: 1118947401.

    """
    dNdlogdp = lognormal_number_distribution(number_concentration,
                                             standard_deviation,
                                             median_particle_diameter,
                                             particle_diameter) * np.log(10) * particle_diameter
    return dNdlogdp


def geometric_mean_diameter(diameter: npt.ArrayLike, conc_per_bin: npt.ArrayLike) -> float:
    """
    Geometric mean diameter as defined by Kulkarni2011, Eq. 4-10.

    Parameters
    ----------
    diameter : npt.ArrayLike
        Particle diameters.
    conc_per_bin : npt.ArrayLike
        Concentration per bin.

    Returns
    -------
    float
        Geometric mean diameter.

    """
    total_number_of_particles = sum(conc_per_bin)
    geo_mean_dia = np.exp(1 / total_number_of_particles * sum(np.log(diameter) * conc_per_bin))
    return geo_mean_dia


def surface_median_diameter(geometric_mean_diameter: float,
                            geometric_standard_deviation: float) -> float:
    """
    Surface median diameter as defined by Kulkarni2011, Eq. 4-11.

    Parameters
    ----------
    geometric_mean_diameter : float
        Geometric mean diameter.
    geometric_standard_deviation : float
        Geometric standard deviation.

    Returns
    -------
    float
        Surface median diameter.

    """
    SMD = geometric_mean_diameter * np.exp(2 * np.log10(geometric_standard_deviation)**2)
    return SMD


def volume_median_diameter(geometric_mean_diameter: float,
                           geometric_standard_deviation: float) -> float:
    """
    Volume median diameter as defined by Kulkarni2011, Eq. 4-11.

    Parameters
    ----------
    geometric_mean_diameter : float
        Geometric mean diameter.
    geometric_standard_deviation : float
        Geometric standard deviation.

    Returns
    -------
    float
        Volume median diameter.

    """
    VMD = geometric_mean_diameter * np.exp(3 * np.log10(geometric_standard_deviation)**2)
    return VMD


def mode(geometric_mean_diameter: float, geometric_standard_deviation: float) -> float:
    """
    Mode of lognormal distribution.

    Parameters
    ----------
    geometric_mean_diameter : float
        Geometric mean diameter.
    geometric_standard_deviation : float
        Geometric standard deviation.

    Returns
    -------
    float
        Mode of lognormal distribution.

    """
    mode = geometric_mean_diameter * np.exp(- np.log10(geometric_standard_deviation)**2)
    return mode


def inas_density(inp_data, conc_sd, surface_sd):
    if isinstance(conc_sd, pd.DataFrame) & isinstance(surface_sd, pd.DataFrame):
        tot_aer = surface_sd.sum(axis=1) / conc_sd.sum(axis=1) * 1e-8  # cm_aerosol^2
        inas = - np.log(1 - inp_data / conc_sd.sum(axis=1)) / tot_aer
    elif isinstance(conc_sd, pd.Series) & isinstance(surface_sd, pd.Series):
        tot_aer = surface_sd.sum(axis=0) / conc_sd.sum(axis=0) * 1e-8  # cm_aerosol^2
        inas = - np.log(1 - inp_data / conc_sd.sum(axis=0)) / tot_aer
    return inas
