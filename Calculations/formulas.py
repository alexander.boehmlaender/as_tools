# -*- coding: utf-8 -*-
"""
Created on Fri Mar  4 14:23:49 2022

@author: st5536
Collection of functions to describe aerosols in gases.

Hallo!
"""

from scipy import constants
import numpy as np
import numpy.typing as npt
import typing
from chemicals.elements import molecular_weight, simple_formula_parser
from chemicals import volume, critical, acentric, dippr, identifiers, interface


def enhancement_factor(ambient_pressure: float, ambient_temperature: float) -> float:
    """
    Calculate enhancement factor for the mole fraction of water vapour, given by [1]_.

    Parameters
    ----------
    ambient_pressure : float
        Ambient pressure in Pa.
    ambient_temperature : float
        Ambient temperature in K.

    Returns
    -------
    float
        Enhancement factor (dimensionless).

    References
    ----------
    .. [1] Picard, A. and Davis, R. S. and Gläser, M. and Fujii, K.
           Revised formula for the density of moist air (CIPM-2007) 2008-02
           Metrologia , Vol. 45, No. 2 p. 149
    """
    p = ambient_pressure  # Pa
    T = ambient_temperature - 273.15  # °C
    alpha = 1.00062  # -
    beta = 3.14e-8  # Pa-1
    gamma = 5.6e-7  # K-2
    f = alpha + beta * p + gamma * T**2  # -
    return f


def mole_fraction_of_water_vapour(ambient_pressure: float, ambient_temperature: float,
                                  relative_humidity: float) -> float:
    """
    Calculate mole fraction of water vapour as given by [1]_.

    Parameters
    ----------
    ambient_pressure : float
        Ambient pressure in Pa.
    ambient_temperature : float
        Ambient temperature in K.
    relative_humidity : float
        Relative humidity, range = (0 ... 1), dimensionless.

    Returns
    -------
    float
        Mole fraction of water vapour (dimensionless).

    References
    ----------
    .. [1] Picard, A. and Davis, R. S. and Gläser, M. and Fujii, K.
           Revised formula for the density of moist air (CIPM-2007) 2008-02
           Metrologia , Vol. 45, No. 2 p. 149
    """
    A = 1.2378847e-5  # K-2
    B = - 1.9121316e-2  # K-1
    C = 33.93711047  # -
    D = -6.3431645e3  # K
    T = ambient_temperature  # K
    p = ambient_pressure  # Pa
    h = relative_humidity  # -
    f = enhancement_factor(ambient_pressure, ambient_temperature)  # -
    p_sv = np.exp(A * T**2 + B * T + C + D / T)
    x_v = h * f * p_sv / p
    return x_v


def compressibility_factor(ambient_pressure: float, ambient_temperature: float,
                           relative_humidity: float) -> float:
    """
    Compressibility factor of air as given by [1]_.

    Parameters
    ----------
    ambient_pressure : float
        Ambient pressure in Pascal.
    ambient_temperature : float
        Ambient temperature in K.
    relative_humidity : float
        Relative humidity, range = (0 ... 1), dimensionless.

    Returns
    -------
    float
        Compressibility factor of air (dimensionless).

    References
    ----------
    .. [1] Picard, A. and Davis, R. S. and Gläser, M. and Fujii, K.
           Revised formula for the density of moist air (CIPM-2007) 2008-02
           Metrologia , Vol. 45, No. 2 p. 149
    """
    a0 = 1.58123e-6  # K Pa-1
    a1 = -2.9331e-8  # Pa-1
    a2 = 1.1043e-10  # K-1 Pa-1
    b0 = 5.707e-6  # K Pa-1
    b1 = -2.051e-8  # Pa-1
    c0 = 1.9898e-4  # K Pa-1
    c1 = -2.376e-6  # Pa-1
    d = 1.83e-11  # K2 Pa-2
    e = -0.765e-8  # K2 Pa-2
    T = ambient_temperature  # K
    p = ambient_pressure  # Pa
    x_v = mole_fraction_of_water_vapour(ambient_pressure, ambient_temperature, relative_humidity)
    Z = (1 - p / T * (a0 + a1 * T + a2 * T**2 + (b0 + b1 * T) * x_v
                      + (c0 + c1 * T) * x_v**2)
         + p**2 / T**2 * (d + e * x_v**2))
    return Z


def density_of_air(ambient_pressure: float,
                   ambient_temperature: float,
                   relative_humidity: float,
                   molar_fraction_CO2: float = None) -> float:
    """
    Calculate density of moist air.

    Parameters
    ----------
    ambient_pressure : float
        Ambient pressure in Pa.
    ambient_temperature : float
        Ambient temperature in K.
    relative_humidity : float
        Relative humidity, range = (0 ... 1), dimensionless.
    molar_fraction_CO2 : float, optional
        Molar fraction of CO2 (dimensionless). The default is None.

    Returns
    -------
    float
        Density of air in kg / m3.

    Note
    ----
    The values are valid for the ranges:
    600 hPa < ambient pressure < 1100 hPa
    15 °C < ambient temperature < 27 °C
    equivalent to
    288 K < ambient temperature < 300 K.
    Given by [1]_.
    The molar fraction of CO2 for the Earth´s atmosphere is
    around 417 ppm (April 2022). This would result in the following
    molar_fraction_C02=417e-6.

    References
    ----------
    .. [1] Picard, A. and Davis, R. S. and Gläser, M. and Fujii, K.
           Revised formula for the density of moist air (CIPM-2007) 2008-02
           Metrologia , Vol. 45, No. 2 p. 149
    """
    if ambient_temperature < 288 or ambient_temperature > 300:
        print('Temperature is outside of the valid range (288, 300) K given by CIPM2007 for '
              'the density of moist air (density_of_air()).')
    if ambient_pressure < 60000 or ambient_pressure > 110000:
        print('Pressure is outside of the valid range (60000, 110000) Pa given by CIPM2007 for '
              'the density of moist air (density_of_air()).')
    if molar_fraction_CO2:
        molar_mass_of_ambient_air = (28.96546
                                     + 12.011 * (molar_fraction_CO2 - 0.004)) * 1e-3  # kg / mol
    else:
        molar_mass_of_ambient_air = 28.9656 * 1e-3  # kg / mol
    molecular_gas_constant = constants.R
    molar_mass_of_water_vapour = molecular_weight(simple_formula_parser('H2O')) * 1e-3  # kg / mol
    p = ambient_pressure
    Z = compressibility_factor(ambient_pressure, ambient_temperature, relative_humidity)
    R = molecular_gas_constant
    T = ambient_temperature
    M_v = molar_mass_of_water_vapour
    x_v = mole_fraction_of_water_vapour(ambient_pressure, ambient_temperature, relative_humidity)
    M_a = molar_mass_of_ambient_air
    rho_air = p / (Z * R * T) * M_a * (1 - x_v * (1 - M_v / M_a))
    return rho_air


def reynolds_number(flow: float, pipe_diameter: float,
                    characteristic_diameter: float,
                    ambient_temperature: float, density: float,
                    verbose=True) -> float:
    """
    Calculate Reynolds number as given by [1], p. 16.
    The Reynolds number gives the measure of ratio of inertial
    forces to viscous forces in a fluid flow and is often used
    to describe flow conditions in aerosol systems.

    Parameters
    ----------
    flow : float
        Volume flow in l/min.
    pipe_diameter : float
        Diameter of pipe in m.
    characteristic_diameter : float
        Characteristic diameter in m. The characteristic diameter is for example
        the diameter of a pipe (Reynolds number for a flow) or the diameter of an
        aerosol particle (Reynolds number for a particle).
    ambient_temperature : float
        Ambient temperature in K.
    density : float
        Density in kg / m3. For humid air given by function density_of_air().
    verbose : bool
        If True prints out Reynolds number as name of regime.

    Returns
    -------
    float
        Reynolds number (dimensionless).

    References
    ----------
    .. [1] Kulkarni, P. (2011). Aerosol measurement : principles, techniques, and applications.
           Ed. by P. Kulkarni, P. A. Baron, and K. Willeke. Hoboken, N.J: Wiley.
           ISBN: 9780470387412.
    """
    average_velocity = face_velocity(flow, pipe_diameter)
    kinematic_eta = kinematic_viscosity(ambient_temperature, density)
    Re = average_velocity * characteristic_diameter / kinematic_eta
    if verbose:
        if Re < 2300:
            print(Re, 'laminar')
        elif Re > 2300 and Re < 4000:
            print(Re, 'transitional')
        elif Re > 4000:
            print(Re, 'turbulent')
    return Re


def viscosity(ambient_temperature: float) -> float:
    """
    Viscosity as calculated by [1], p. 18.

    Parameters
    ----------
    ambient_temperature : float
        Ambient temperature in K.

    Returns
    -------
    float
        Viscosity of gas or aerosol in Pa s.

    References
    ----------
    .. [1] Kulkarni, P. (2011). Aerosol measurement : principles, techniques, and applications.
           Ed. by P. Kulkarni, P. A. Baron, and K. Willeke. Hoboken, N.J: Wiley.
           ISBN: 9780470387412.
    """
    T = ambient_temperature  # K
    eta_ref = 18.203e-6  # Pa s
    T_f = 293.15  # K
    Su = 110.4  # K
    eta = eta_ref * (T_f + Su) / (T + Su) * (T / T_f)**(3/2)  # Pa s
    return eta


def kinematic_viscosity(ambient_temperature: float, density: float = 1000) -> float:
    """
    Kinematic viscosity as calculated by [1], p. 18.

    Parameters
    ----------
    ambient_temperature : float
        Ambient temperature in K.
    density : float, optional
        Density of gas or aerosol in kg / m3. The default is 1000.

    Returns
    -------
    float
        Kinematic viscosity of gas or aerosol in Pa s m3 / kg.

    References
    ----------
    .. [1] Kulkarni, P. (2011). Aerosol measurement : principles, techniques, and applications.
           Ed. by P. Kulkarni, P. A. Baron, and K. Willeke. Hoboken, N.J: Wiley.
           ISBN: 9780470387412.
    """
    T = ambient_temperature  # K
    rho = density  # kg / m3
    eta = viscosity(ambient_temperature=T)
    kinematic_eta = eta / rho
    return kinematic_eta


def mean_free_path(ambient_pressure: float, ambient_temperature: float) -> float:
    """
    Mean free path as calculated by [1], p. 19.
    Mean free path is the mean distance a molecule travels
    before colliding with another molecule.

    Parameters
    ----------
    ambient_pressure : float
        Ambient pressure in Pa.
    ambient_temperature : float
        Ambient Temperature in K.

    Returns
    -------
    float
        Mean free path in m.

    References
    ----------
    .. [1] Kulkarni, P. (2011). Aerosol measurement : principles, techniques, and applications.
           Ed. by P. Kulkarni, P. A. Baron, and K. Willeke. Hoboken, N.J: Wiley.
           ISBN: 9780470387412.
    """
    p = ambient_pressure
    T = ambient_temperature
    lambda_ref = 0.0665e-6  # m
    Su = 110.4  # K
    lambda_ = lambda_ref * (101300 / p) * (T / 293.15) * ((1 + Su / 293.15)/(1 + Su / T))  # m
    return lambda_


def knudsen_number(particle_diameter: float,
                   ambient_pressure: float,
                   ambient_temperature: float) -> float:
    """
    Knudsen number as calculated by [1], p. 19.
    The Knudsen number relates the gas molecular mean
    free path to the physical dimension of the particle.
    Kn << 1 indicates continuum flow and Kn >> 1 indicates
    free molecular flow. In between is the transition or
    slip flow regime.

    Parameters
    ----------
    particle_diameter : float
        Particle diameter in m.
    ambient_pressure : float
        Ambient pressure in Pa.
    ambient_temperature : float
        Ambient temperature in K.

    Returns
    -------
    float
        Knudsen number (dimensionless).

    References
    ----------
    .. [1] Kulkarni, P. (2011). Aerosol measurement : principles, techniques, and applications.
           Ed. by P. Kulkarni, P. A. Baron, and K. Willeke. Hoboken, N.J: Wiley.
           ISBN: 9780470387412.
    """
    lambda_ = mean_free_path(ambient_pressure=ambient_pressure,
                             ambient_temperature=ambient_temperature)
    d_p = particle_diameter
    Kn = 2 * lambda_ / d_p  # -
    return Kn


def cunningham_slip_correction(particle_diameter: float,
                               ambient_pressure: float,
                               ambient_temperature: float) -> float:
    """
    Cunningham slip correction factor as calculated by [1], p. 115
    Cunningham slip correction factor is used to account for noncontinuum
    effects when calculating the drag on small particles. It becomes significant
    when particles become smaller than 15 µm.

    Parameters
    ----------
    particle_diameter : float
        Particle diameter in m.
    ambient_pressure : float
        Ambient pressure in Pa.
    ambient_temperature : float
        Ambient temperature in K.

    Returns
    -------
    float
        Cunningham slip correction factor (dimensionless).

    References
    ----------
    .. [1] Kulkarni, P. (2011). Aerosol measurement : principles, techniques, and applications.
           Ed. by P. Kulkarni, P. A. Baron, and K. Willeke. Hoboken, N.J: Wiley.
           ISBN: 9780470387412.
    """
    lambda_ = mean_free_path(ambient_pressure=ambient_pressure,
                             ambient_temperature=ambient_temperature)  # m
    d_p = particle_diameter
    C_c = 1 + lambda_ / d_p * (2.33 + 0.966 * np.exp(- 0.499 * d_p / lambda_))  # -
    return C_c


def particle_diffusion_coefficient(particle_diameter: float,
                                   ambient_pressure: float,
                                   ambient_temperature: float) -> float:
    """
    Particle diffusion coefficient as calculated by [1], p. 115.

    Parameters
    ----------
    particle_diameter : float
        Particle diameter in m.
    ambient_pressure : float
        Ambient pressure in Pa.
    ambient_temperature : float
        Ambient temperature in K.

    Returns
    -------
    float
        Particle diffusion coefficient in m2 / s.

    References
    ----------
    .. [1] Kulkarni, P. (2011). Aerosol measurement : principles, techniques, and applications.
           Ed. by P. Kulkarni, P. A. Baron, and K. Willeke. Hoboken, N.J: Wiley.
           ISBN: 9780470387412.
    """
    T = ambient_temperature
    p = ambient_pressure
    d_p = particle_diameter
    eta = viscosity(ambient_temperature=T)  # Pa s
    C_c = cunningham_slip_correction(particle_diameter=d_p,
                                     ambient_pressure=p,
                                     ambient_temperature=T)  # -
    D = (constants.Boltzmann * T) / (3 * np.pi * eta * d_p) * C_c  # m2 / s
    return D


def electrical_mobility(particle_diameter: float,
                        ambient_pressure: float,
                        ambient_temperature: float,
                        number_of_charges: int) -> float:
    """
    Particle mobility due to an electric field.
    Used in migraction_velocity().
    From [1], p. 340.

    Parameters
    ----------
    particle_diameter : float
        Particle diameter in m.
    ambient_pressure : float
        Ambient pressure in Pa.
    ambient_temperature : float
        Ambient temperature in K.
    number_of_charges : int
        Number of charges per aerosol particle.

    Returns
    -------
    float
        Particle mobility in m2 / (V s).

    References
    ----------
    .. [1] Kulkarni, P. (2011). Aerosol measurement : principles, techniques, and applications.
           Ed. by P. Kulkarni, P. A. Baron, and K. Willeke. Hoboken, N.J: Wiley.
           ISBN: 9780470387412.
    """
    em = (number_of_charges * constants.e
          * cunningham_slip_correction(particle_diameter, ambient_pressure, ambient_temperature)
          / (3 * np.pi * viscosity(ambient_temperature) * particle_diameter))
    return em


def electrical_migration_velocity(particle_diameter: float,
                                  ambient_pressure: float,
                                  ambient_temperature: float,
                                  number_of_charges: int,
                                  electric_field_strength: float) -> float:
    """
    The migration velocity of a charged particle inside an electric field.
    From [1]_, p. 341.

    Parameters
    ----------
    particle_diameter : float
        Particle diameter in m.
    ambient_pressure : float
        Ambient pressure in Pa.
    ambient_temperature : float
        Ambient temperature in K.
    number_of_charges : int
        Number of charges per aerosol particle.
    electric_field_strength : float
        Strength of the electric field in V / m.

    Returns
    -------
    float
        Migration velocity in m / s.

    References
    ----------
    .. [1] Kulkarni, P. (2011). Aerosol measurement : principles, techniques, and applications.
           Ed. by P. Kulkarni, P. A. Baron, and K. Willeke. Hoboken, N.J: Wiley.
           ISBN: 9780470387412.
    """
    d_p = particle_diameter  # in m
    p = ambient_pressure  # in Pa
    T = ambient_temperature  # in K
    n = number_of_charges  # in -
    em = electrical_mobility(particle_diameter=d_p,
                             ambient_pressure=p,
                             ambient_temperature=T,
                             number_of_charges=n)  # in m2 / (V s)
    velocity_mig = em * electric_field_strength  # in m / s
    return velocity_mig


def face_velocity(flow: float, diameter: float) -> float:
    """
    Face velocity as calculated by [1] for a tube of diameter = filter_diameter.

    Parameters
    ----------
    flow : float
        Volume flow in l/min.
    diameter : float
        Diameter of tube in m.

    Returns
    -------
    float
        Mean velocity at a specific flow through a tube of specific diameter in m/s.

    References
    ----------
    .. [1] Xiang, M. et al. (Dec. 2020). “Comparative review of efficiency analysis
           for airborne solid sub-micrometer particle sampling by nuclepore filters”.
           In: Chemical Engineering Research and Design 164, pp. 338–351.
           doi: 10.1016/j.cherd.2020.10.009.
    """
    Q = 1e-3 / 60 * flow  # l / min -> m3 / s
    d_f = diameter  # m
    U_0 = Q / (np.pi / 4 * d_f**2)  # m / s
    return U_0


def particle_relaxation_time(particle_diameter: float, particle_density: float,
                             ambient_pressure: float, ambient_temperature: float) -> float:
    """
    Particle relaxation time as calculated by [1], p. 25.
    This is the time a particle takes to reach (1 - 1/e) or 0.63 of
    its final velocity when subjected to an external force field.

    Parameters
    ----------
    particle_diameter : float
        Particle diameter in m.
    particle_density : float
        Particle density in kg / m3.
    ambient_pressure : float
        Ambient pressure in Pa.
    ambient_temperature : float
        Ambient temperature in K.

    Returns
    -------
    float
        Particle relaxation time in s.

    References
    ----------
    .. [1] Kulkarni, P. (2011). Aerosol measurement : principles, techniques, and applications.
           Ed. by P. Kulkarni, P. A. Baron, and K. Willeke. Hoboken, N.J: Wiley.
           ISBN: 9780470387412.
    """
    d_p = particle_diameter
    p = ambient_pressure
    T = ambient_temperature
    rho_p = particle_density  # kg / m3
    eta = viscosity(ambient_temperature=T)  # Pa s
    C_c = cunningham_slip_correction(particle_diameter=d_p,
                                     ambient_pressure=p,
                                     ambient_temperature=T)  # -
    tau = rho_p * d_p**2 / (18 * eta) * C_c  # s
    return tau


def stokes_number(particle_diameter: float, particle_density: float,
                  pore_diameter: float, flow: float,
                  filter_diameter: float, ambient_pressure: float,
                  ambient_temperature: float) -> float:
    """
    Stokes number as calculated by [1], p. 25.
    When flow conditions change suddenly, as in case of
    the particle traveling toward the collection
    surface of an impactor, the ratio of the stopping distance S
    to a characteristic dimension, d, is defined as the Stokes
    number, Stk.

    Parameters
    ----------
    particle_diameter : float
        Particle diameter in m.
    pore_diameter : float
        Pore diameter in m.
    flow : float
        Volume flow in l/min.
    filter_diameter : float
        Filter diameter.
    ambient_pressure : float
        Ambient pressure in Pa.
    ambient_temperature : float
        Ambient temperature in K.

    Returns
    -------
    float
        Stokes number (dimensionless).

    References
    ----------
    .. [1] Kulkarni, P. (2011). Aerosol measurement : principles, techniques, and applications.
           Ed. by P. Kulkarni, P. A. Baron, and K. Willeke. Hoboken, N.J: Wiley.
           ISBN: 9780470387412.
    """
    d_p = particle_diameter
    d_pore = pore_diameter
    Q = flow
    d_f = filter_diameter
    p = ambient_pressure
    T = ambient_temperature
    rho_p = particle_density
    U_0 = face_velocity(Q, d_f)  # m / s
    tau = particle_relaxation_time(particle_diameter=d_p,
                                   particle_density=rho_p,
                                   ambient_pressure=p,
                                   ambient_temperature=T)  # s
    S = U_0 * tau  # m
    Stk = S / d_pore  # -
    return Stk


def terminal_settling_velocity(particle_diameter: float,
                               particle_density: float,
                               ambient_pressure: float,
                               ambient_temperature: float) -> float:
    """
    Terminal velocity for a particle under the influence
    of two forces.

    Parameters
    ----------
    particle_diameter : float
        Particle diameter in m.
    particle_density : float
        Particle density in kg / m3.
    ambient_pressure : float
        Ambient pressure in Pa.
    ambient_temperature : float
        Ambient temperature in K.

    Returns
    -------
    float
        Terminal settling velocity in m / s.

    References
    ----------
    .. [1] Kulkarni, P. (2011). Aerosol measurement : principles, techniques, and applications.
           Ed. by P. Kulkarni, P. A. Baron, and K. Willeke. Hoboken, N.J: Wiley.
           ISBN: 9780470387412.
    """
    tau = particle_relaxation_time(particle_diameter=particle_diameter,
                                   particle_density=particle_density,
                                   ambient_pressure=ambient_pressure,
                                   ambient_temperature=ambient_temperature)  # s
    V_ts = tau * constants.g
    return V_ts


def vapor_pressure_cpc_working_fluids(temperature: float, fluid: str) -> float:
    """
    Calculate saturation vapor pressure for common
    CPC working fluids. Values are taken from
    [1], p. 382. The saturation vapor pressure
    is defined as the equilibrium partial vapor
    pressure for a flat liquid surface.

    Parameters
    ----------
    temperature : float
        Temperature in K.
    fluid : str
        Name of working fluid. Supported options
        are ['water', 'methanol', 'ethanol',
             'n-buthanol', 'dibutyl-phthalate'].

    Returns
    -------
    vapor_pressure : float
        Vapor pressure in Pa.

    References
    ----------
    .. [1] Kulkarni, P. (2011). Aerosol measurement : principles, techniques, and applications.
           Ed. by P. Kulkarni, P. A. Baron, and K. Willeke. Hoboken, N.J: Wiley.
           ISBN: 9780470387412.
    """
    match fluid:
        case 'water':
            a = 10.23  # -
            b = 1750  # K
            c = 38  # K
            vapor_pressure = 10**(a - b / (temperature - c))
        case 'methanol':
            a = 10.00  # -
            b = 1233  # K
            c = 45  # K
            vapor_pressure = 10**(a - b / (temperature - c))
        case 'ethanol':
            a = 10.16  # -
            b = 1554  # K
            c = 50.2  # K
            vapor_pressure = 10**(a - b / (temperature - c))
        case 'n-buthanol':
            b = 46.78  # K
            c = 11.26  # K
            vapor_pressure = 10**(-52.3 * b / temperature + c)
        case 'dibutyl-phthalate':
            a = 18.39  # K
            b = 5099  # K
            c = 109  # K
            vapor_pressure = 10**(a - b / (temperature - c))
    return vapor_pressure


def surface_tension_of_water(temperature: float, salt: bool = False,
                             salinity: float = None) -> float:
    """
    Surface tension of water as given by [1].
    Valid between triple point and critical temperature,
    i.e. 273.15 K -> 647.096 K. Also reasonable accurate
    until at least -25 °C (248.15 K) for supercooled
    water.

    Surface tension of seawater as given by [2].
    Valid for the following ranges:
    0 <= salinity <= 131 g / kg for 1 °C (274.15 K) <= temperature <= 92 °C (365.15 K)
    0 <= salinity <= 38 g / kg for -25 °C (248.15 K) <= temperature <= 1 °C (274.15 K)

    Parameters
    ----------
    temperature : float
        Temperature in K.
    salt : bool, optional
        If True return the surface tension of seawater.
        The default is False.
    salinity : float, optional
        Salinity of the sea water. Only needed if salt
        is True. The default is None.
        Reference sea water has a salinity of 35.16504 g / kg.

    Returns
    -------
    surface_tension : float
        Surface tension in N / m.

    References
    ----------
    .. [1] IAPWS R1-76(2014) Revised Release on Surface Tension
           of Ordinary Water Substance June 2014
    .. [2] IAPWS G14-19 Guideline on the Surface Tension of Seawater
           October 2019
    """
    B = 235.8e-3  # N / m
    critical_temperature = 647.096  # K
    tau = 1 - temperature / critical_temperature  # -
    mu = 1.256  # -
    b = - 0.625  # -
    surface_tension = B * tau**mu * (1 + b * tau)
    if salt:
        a1 = 3.766e-4  # kg / g
        a2 = 2.347e-6  # kg / (g °C)
        surface_tension_seawater = surface_tension * (1 + a1 * salinity
                                                      + a2 * salinity * (temperature + 273.15))
        return surface_tension_seawater
    else:
        return surface_tension


def vapor_pressure_of_water(temperature: float) -> float:
    """
    Vapor pressure of water as given by [1].
    Valid for the following range:
    273.15 K <= temperature <= 647.096 K.

    Parameters
    ----------
    temperature : float
        Temperature in K.

    Returns
    -------
    vapor_pressure : float
        Vapor pressure of water in Pa.

    References
    ----------
    .. [1] IAPWS SR1-86(1992) Revised Supplementary Release on
           Saturation Properties of Ordinary Water Substance
           September 1992
    """
    critical_temperature = 647.096  # K
    critical_pressure = 22.064  # MPa
    tau = 1 - temperature / critical_temperature  # -
    a1 = -7.85951783  # -
    a2 = 1.84408259  # -
    a3 = -11.7866479  # -
    a4 = 22.6807411  # -
    a5 = -15.9618719  # -
    a6 = 1.80122502  # -
    vapor_pressure = (critical_pressure
                      * np.exp((critical_temperature / temperature)
                               * (a1 * tau
                                  + a2 * tau**(3/2)
                                  + a3 * tau**3
                                  + a4 * tau**(7/2)
                                  + a5 * tau**4
                                  + a6 * tau**(15/2)
                                  )
                               )
                      )
    return vapor_pressure * 1e6


def density_of_water(temperature: float) -> float:
    """
    Density of saturated water as given by [1]_.
    Valid for the following range:
    273.15 K <= temperature <= 647.096 K.

    Parameters
    ----------
    temperature : float
        Temperature in K.

    Returns
    -------
    density_water : float
        Density of water in kg / m3.

    References
    ----------
    .. [1] IAPWS SR1-86(1992) Revised Supplementary Release on
           Saturation Properties of Ordinary Water Substance
           September 1992
    """
    critical_temperature = 647.096  # K
    critical_density = 322  # kg/m3
    tau = 1 - temperature / critical_temperature  # -
    b1 = 1.99274064  # K3
    b2 = 1.09965342  # K3/2
    b3 = -0.510839303  # K3/5
    b4 = -1.75493479  # K3/16
    b5 = -45.5170352  # K3/43
    b6 = -6.74694450e5  # K3/110
    density_water = critical_density * (1 + b1 * tau**(1/3)
                                        + b2 * tau**(2/3)
                                        + b3 * tau**(5/3)
                                        + b4 * tau**(16/3)
                                        + b5 * tau**(43/3)
                                        + b6 * tau**(110/3))
    return density_water


def molar_volume_water(density_water: float) -> float:
    """
    Molar volume of water.

    Parameters
    ----------
    density_water : float
        Density of water in kg / m3.
        Can be calculated by function density_of_water().

    Returns
    -------
    molar_volume : float
        Molar volume of water in m3 / mol.
    """
    molar_mass_of_water_vapour = 18.01528 * 1e-3  # kg / mol
    molar_volume = molar_mass_of_water_vapour / density_water
    return molar_volume


def homogeneous_ice_nucleation_curve(pressure: float,
                                     temperature: float) -> float:
    """
    Homogeneous ice nucleation curve for µm sized samples.
    From [1].

    Parameters
    ----------
    pressure : float
        Pressure in Pa.
    temperature : float
        Temperature in K.

    Returns
    -------
    float
        Homogeneous ice nucleation curve for
        supercooled water.

    References
    ----------
    .. [1] Vincent Holten, Jan V. Sengers, Mikhail A. Anisimov;
           Equation of State for Supercooled Water at Pressures up to 400 MPa.
           J. Phys. Chem. Ref. Data 1 December 2014; 43 (4): 043101.
           https://doi.org/10.1063/1.4895593
    """
    pressure = pressure * 1e-6  # Pa --> MPa
    theta = temperature / 235.15  # K --> -
    if pressure < 198.9:
        pressure_hom = 0.1 + 228.27 * (1 - theta**(6.243)) + 15.724 * (1 - theta**(79.81))
        return pressure_hom
    elif pressure > 198.9 and pressure < 1500:
        temperature_hom = (172.82 + 0.03718 * pressure
                           + 3.403e-5 * pressure**2
                           - 1.573e-8 * pressure**3)
        return temperature_hom


def density_supercooled_water(ambient_temperature: float) -> float:
    """
    Density of supercooled water is fitted to
    data from [1]_ via scipy.optimize.curve_fit().

    Parameters
    ----------
    ambient_temperature : float
        Temperature in K.

    Returns
    -------
    float
        Mass density of supercooled water in kg / m3.

    References
    ----------
    .. [1] Haynes, William M., Lide, David R. and Bruno, Thomas J.. CRC Handbook of
           Chemistry and Physics : a ready-reference book of chemical and physical data.
           97th edition: CRC press, 2017.
    """
    return dippr.EQ105(ambient_temperature, 1.00103204, 1.00059245, 33.14156398, 5.18590732) * 1e3


def flow_profile(face_velocity: float, radius: float, distance_from_middle: float,
                 reynolds_number: float) -> tuple[float, float, float]:
    """
    Generalized power law for the flow profile inside a pipe of
    a specific radius. Taken from [1].

    Parameters
    ----------
    face_velocity : float
        Face velocity in m / s.
    radius : float
        Radius of pipe in m.
    distance_from_middle : float
        Distance from the middle of the pipe in m.
    reynolds_number : float
        Reynolds number (dimensionless).

    Returns
    -------
    tuple[float, float, float]
        First element is velocity of flow at a given point
        inside a pipe for laminar and turbulent flow.
        Second and third element are needed to calculate
        the mean velocity for (turbulent) flow.

    References
    ----------
    .. [1] Salama, A. Velocity Profile Representation for Fully Developed
           Turbulent Flows in Pipes: A Modified Power Law. Fluids 2021, 6, 369.
           https://doi.org/10.3390/fluids6100369

    """
    if reynolds_number < 2300:
        n, m = 1, 2
    else:
        n, m = 0.77 * np.log(reynolds_number) - 3.47, 2
    velocity = 2 * face_velocity * (1 - (distance_from_middle / radius)**m)**(1 / n)
    return velocity, n, m


def face_velocity_mean(face_velocity: float, n: float, m: float) -> float:
    """
    Mean velocity for flows inside a pipe.
    For laminar flow: n, m = 1, 2. From [1].

    Parameters
    ----------
    face_velocity : float
        Face velocity in m / s.
    n : float
        Exponent for generalized power law.
        See flow_profile().
    m : float
        Exponent for generalized power law.
        See flow_profile().

    Returns
    -------
    float
        Mean velocity inside a pipe.
        For laminar flow reduces to face velocity.

    References
    ----------
    .. [1] Salama, A. Velocity Profile Representation for Fully Developed
           Turbulent Flows in Pipes: A Modified Power Law. Fluids 2021, 6, 369.
           https://doi.org/10.3390/fluids6100369
    """
    from scipy.special import beta
    face_velocity_mean = 2 * face_velocity * (2 * beta(2/m, (n+1)/n) / m)
    return face_velocity_mean


def molecular_volume(temperature: float = None,
                     cas_number: str = '7732-18-5',
                     method: str = None) -> float:
    """
    Calculate molecular volume for different liquids.

    Parameters
    ----------
    temperature : typing.Optional[float], optional
        Temperature of liquid in K. The default is None.
    cas_number : str, optional
        CAS number. The default is '7732-18-5' (water).
    method : str, optional
        Specify a single method. The default is None.

    Raises
    ------
    NotImplementedError
        If the method specified is not in list method_lst.
    ValueError
        If the method specified does not contain the chemical
        with the given cas_number.

    Returns
    -------
    float
        Volume of one molecule in m3.

    Note
    ----
        The two methods CRC_inorg_l_const and
        CRC_inorg_s_const return constant values,
        the temperature value therefore has no
        influence on the returned value.


    Example
    -------
    >>> molecular_volume(temperature=273, cas_number='7732-18-5', method='COSTALD')
    2.266258318331472e-29
    """
    df_lst = [volume.rho_data_COSTALD,
              volume.rho_data_SNM0,
              volume.rho_data_Perry_8E_105_l,
              volume.rho_data_VDI_PPDS_2,
              volume.rho_data_CRC_inorg_l,
              volume.rho_data_CRC_inorg_l_const,
              volume.rho_data_CRC_inorg_s_const]

    method_lst = ['COSTALD', 'SNM0', 'Perry', 'VDI', 'CRC_inorg_l', 'CRC_inorg_l_const',
                  'CRC_inorg_s_const']

    if not method:
        for i, df in enumerate(df_lst):
            if df[df.index == cas_number].empty:
                continue
            else:
                idx, parameters = i, df[df.index == cas_number]
            break
    else:
        for i, value in enumerate(method_lst):
            if method not in method_lst:
                raise NotImplementedError(f'Method {method} does not exist. '
                                          'Choose a different '
                                          f'method from {method_lst}')
            if method == value:
                if not df_lst[i][df_lst[i].index == cas_number].empty:
                    idx, parameters = i, df_lst[i][df_lst[i].index == cas_number]
                else:
                    method_lst.remove(method)
                    raise ValueError(f'Method {method} does not contain data for '
                                     f'CAS number {cas_number}. Choose a different '
                                     f'method from {method_lst}')

    match idx:
        case 0:
            return volume.COSTALD(T=temperature, Tc=critical.Tc(cas_number),
                                  Vc=parameters['Vchar'].to_numpy()[0],
                                  omega=parameters['omega_SRK'].to_numpy()[0]) / constants.Avogadro
        case 1:
            return (volume.SNM0(T=temperature, Tc=critical.Tc(cas_number),
                                Vc=critical.Vc(cas_number), omega=acentric.omega(cas_number),
                                delta_SRK=parameters['delta_SRK'].to_numpy()[0])
                    / constants.Avogadro)

        case 2:
            return dippr.EQ105(T=temperature, A=parameters['C1'].to_numpy()[0],
                               B=parameters['C2'].to_numpy()[0], C=parameters['C3'].to_numpy()[0],
                               D=parameters['C4'].to_numpy()[0])**(-1) / constants.Avogadro
        case 3:
            molar_weight = identifiers.MW(cas_number)
            mass_density = dippr.EQ116(
                T=temperature, Tc=parameters['Tc'].to_numpy()[0],
                A=parameters['A'].to_numpy()[0], B=parameters['B'].to_numpy()[0],
                C=parameters['C'].to_numpy()[0], D=parameters['D'].to_numpy()[0],
                E=parameters['MW'].to_numpy()[0])
            return molar_weight / (mass_density * constants.Avogadro)
        case 4:
            return volume.CRC_inorganic(T=temperature, rho0=parameters['rho'].to_numpy()[0],
                                        k=parameters['k'].to_numpy()[0],
                                        Tm=parameters['Tm'].to_numpy()[0],
                                        MW=parameters['MW'].to_numpy()[0]) / constants.Avogadro
        case 5:
            return parameters['Vm'].to_numpy()[0] / constants.Avogadro
        case 6:
            return parameters['Vm'].to_numpy()[0] / constants.Avogadro


def surface_tension(temperature: float, cas_number: str = '71-36-3',
                    method: typing.Optional[str] = None) -> float:
    """
    Surface tension calculation using fit parameters.

    Parameters
    ----------
    temperature : float
        Temperature in K.
    cas_number : str, optional
        CAS number. The default is '71-36-3' (butanol).
    method : typing.Optional[str], optional
        Specify a single method. The default is None.

    Raises
    ------
    NotImplementedError
        If the method specified is not in list method_lst.
    ValueError
        If the method specified does not contain the chemical
        with the given cas_number.

    Returns
    -------
    float
        Surface tension in N / m.

    Note
    ----
    For water the use of the function
    surface_tension_of_water() is preferred
    since it also includes salinity.

    Example
    -------
    >>> surface_tension(temperature=290, cas_number='71-36-3', method='Somayajulu2')
    0.024833305699797057
    """
    df_lst = [interface.sigma_data_Mulero_Cachadina,
              interface.sigma_data_Somayajulu2,
              interface.sigma_data_VDI_PPDS_11]

    method_lst = ['Mulero', 'Somayajulu2', 'VDI']

    if not method:
        for i, df in enumerate(df_lst):
            if df[df.index == cas_number].empty:
                continue
            else:
                idx, parameters = i, df[df.index == cas_number]
            break
    else:
        for i, value in enumerate(method_lst):
            if method not in method_lst:
                raise NotImplementedError(f'Method {method} does not exist. '
                                          'Choose a different '
                                          f'method from {method_lst}')
            if method == value:
                if not df_lst[i][df_lst[i].index == cas_number].empty:
                    idx, parameters = i, df_lst[i][df_lst[i].index == cas_number]
                else:
                    method_lst.remove(method)
                    raise ValueError(f'Method {method} does not contain data for '
                                     f'CAS number {cas_number}. Choose a different '
                                     f'method from {method_lst}')

    match idx:
        case 0:
            return interface.REFPROP_sigma(temperature,
                                           Tc=parameters['Tc'].to_numpy()[0],
                                           sigma0=parameters['sigma0'].to_numpy()[0],
                                           n0=parameters['n0'].to_numpy()[0],
                                           sigma1=parameters['sigma1'].to_numpy()[0],
                                           n1=parameters['n1'].to_numpy()[0],
                                           sigma2=parameters['sigma2'].to_numpy()[0],
                                           n2=parameters['n2'].to_numpy()[0])
        case 1:
            return interface.Somayajulu(temperature,
                                        Tc=parameters['Tc'].to_numpy()[0],
                                        A=parameters['A'].to_numpy()[0],
                                        B=parameters['B'].to_numpy()[0],
                                        C=parameters['C'].to_numpy()[0])
        case 2:
            return dippr.EQ106(T=temperature,
                               Tc=parameters['Tc'].to_numpy()[0],
                               A=parameters['A'].to_numpy()[0],
                               B=parameters['B'].to_numpy()[0])


def eta_tube_grav(particle_diameter: float,
                  ambient_pressure: float,
                  ambient_temperature: float,
                  tube_length: float,
                  tube_diameter: float,
                  volume_flow: float,
                  particle_density: float,
                  theta: float,
                  regime: str) -> float:
    """
    Transport efficiency related to gravitational losses in a tube
    as defined by [1], p.89.

    Parameters
    ----------
    particle_diameter : float
        Particle diameter in m.
    ambient_pressure : float
        Ambient pressure in Pa.
    ambient_temperature : float
        Ambient temperature in K.
    tube_length : float
        Length of tube in m.
    tube_diameter : float
        Diameter of tube in m.
    volume_flow : float
        Volume flow in l/min.
    particle_density : float
        Particle density in kg / m3.
    theta : float
        Angle of inclination.
        0 for a horizontal tube.
        90 for a vertical tube.
    regime : str
        Flow regime for the setup.
        Needs to be either 'laminar'
        or 'turbulent'. The flow regime
        can be checked by calculated the
        Reynolds number (reynolds_number()).

    Returns
    -------
    float
        Transport efficiency for the given parameters.

    References
    ----------
    .. [1] Kulkarni, P. (2011). Aerosol measurement : principles, techniques, and applications.
           Ed. by P. Kulkarni, P. A. Baron, and K. Willeke. Hoboken, N.J: Wiley.
           ISBN: 9780470387412.
    """
    d_p = particle_diameter  # m
    p = ambient_pressure  # Pa
    T = ambient_temperature  # K
    L = tube_length  # m
    d = tube_diameter  # m
    Q = volume_flow  # l / min
    U = face_velocity(Q, d)  # sampling velocity, m / s
    Q = 1e-3 / 60 * Q  # flow, l / min -> m3 / s
    rho_p = particle_density  # kg / m3
    theta = theta / 360 * 2 * np.pi  # angle of inclination, ° -> rad
    V_ts = terminal_settling_velocity(particle_diameter=d_p,
                                      particle_density=rho_p,
                                      ambient_pressure=p,
                                      ambient_temperature=T)  # m / s
    epsilon = 3 / 4 * L / d * V_ts / U * np.cos(theta)  # -

    if regime == 'laminar':
        eta_tube_grav = (1 - 2 / np.pi * (2 * epsilon * np.sqrt(1 - epsilon**(2/3))
                                          - epsilon**(1/3) * np.sqrt(1 - epsilon**(2/3))
                                          + np.arcsin(epsilon**(1/3))))
    elif regime == 'turbulent':
        eta_tube_grav = np.exp(- d * L * V_ts * np.cos(theta) / Q)
    return eta_tube_grav


def xi_func(particle_diameter: float,
            ambient_pressure: float,
            ambient_temperature: float,
            tube_length: float,
            volume_flow: float) -> float:
    """
    Helper function to calculate the xi value as defined in [1], p.90.

    Parameters
    ----------
    particle_diameter : float
        Particle diameter in m.
    ambient_pressure : float
        Ambient pressure in Pa.
    ambient_temperature : float
        Ambient temperature in K.
    tube_length : float
        Length of tube in m.
    volume_flow : float
        Volume flow in l/min.

    Returns
    -------
    float
        Value of xi, used in other functions.

    References
    ----------
    .. [1] Kulkarni, P. (2011). Aerosol measurement : principles, techniques, and applications.
           Ed. by P. Kulkarni, P. A. Baron, and K. Willeke. Hoboken, N.J: Wiley.
           ISBN: 9780470387412.
    """
    d_p = particle_diameter  # m
    p = ambient_pressure  # Pa
    T = ambient_temperature  # K
    L = tube_length  # m
    Q = volume_flow  # l / min
    D = particle_diffusion_coefficient(particle_diameter=d_p,
                                       ambient_pressure=p,
                                       ambient_temperature=T)  # m2 / s
    Q = 1e-3 / 60 * Q  # flow, l / min -> m3 / s
    xi = np.pi * D * L / Q  # -
    return xi


def schmidt(particle_diameter: float,
            ambient_pressure: float,
            ambient_temperature: float,
            density: float) -> float:
    """
    The Schmidt number is the ratio of kinematic viscosity to
    diffusion coefficient as defined by [1], p.23.

    Parameters
    ----------
    particle_diameter : float
        Particle diameter in m.
    ambient_pressure : float
        Ambient pressure in Pa.
    ambient_temperature : float
        Ambient temperature in K.
    density : float
        Particle density in kg / m3.

    Returns
    -------
    float
        Schmidt number (dimensionless).

    References
    ----------
    .. [1] Kulkarni, P. (2011). Aerosol measurement : principles, techniques, and applications.
           Ed. by P. Kulkarni, P. A. Baron, and K. Willeke. Hoboken, N.J: Wiley.
           ISBN: 9780470387412.
    """
    d_p = particle_diameter  # m
    p = ambient_pressure  # Pa
    T = ambient_temperature  # K
    rho = density  # kg / m3
    kinematic_eta = kinematic_viscosity(ambient_temperature=T, density=rho)  # m2 / s
    D = particle_diffusion_coefficient(particle_diameter=d_p,
                                       ambient_pressure=p,
                                       ambient_temperature=T)  # m2 / s
    Sc = kinematic_eta / D  # -
    return Sc


def sherwood(particle_diameter: float,
             ambient_pressure: float,
             ambient_temperature: float,
             tube_length: float,
             tube_diameter: float,
             volume_flow: float,
             relative_humidity: float,
             regime: str) -> float:
    """
    The Sherwood number is a dimensionless mass transfer coefficient
    that contains the diffusive deposition velocity in the definition.
    From [1], p. 90.

    Parameters
    ----------
    particle_diameter : float
        Particle diameter in m.
    ambient_pressure : float
        Ambient pressure in Pa.
    ambient_temperature : float
        Ambient temperature in K.
    tube_length : float
        Length of tube in m.
    tube_diameter : float
        Diameter of tube in m.
    volume_flow : float
        Volume flow in l/min.
    relative_humidity : float
        Relative humidity, range = (0 ... 1), dimensionless.
    regime : str
        Flow regime for the setup.
        Needs to be either 'laminar'
        or 'turbulent'. The flow regime
        can be checked by calculated the
        Reynolds number (reynolds_number()).

    Returns
    -------
    float
        Sherwood number (dimensionless).

    References
    ----------
    .. [1] Kulkarni, P. (2011). Aerosol measurement : principles, techniques, and applications.
           Ed. by P. Kulkarni, P. A. Baron, and K. Willeke. Hoboken, N.J: Wiley.
           ISBN: 9780470387412.
    """
    d_p = particle_diameter  # m
    p = ambient_pressure  # Pa
    T = ambient_temperature  # K
    L = tube_length  # m
    d = tube_diameter  # m
    Q = volume_flow  # l / min
    RH = relative_humidity  # -
    rho = density_of_air(ambient_pressure=p,
                         ambient_temperature=T,
                         relative_humidity=RH)  # kg / m3
    xi = xi_func(particle_diameter=d_p,
                 ambient_pressure=p,
                 ambient_temperature=T,
                 tube_length=L,
                 volume_flow=Q)  # -
    Re_f = reynolds_number(flow=Q,
                           pipe_diameter=d,
                           characteristic_diameter=d,
                           ambient_temperature=T,
                           density=rho)  # -
    Sc = schmidt(particle_diameter=d_p,
                 ambient_pressure=p,
                 ambient_temperature=T,
                 density=rho)  # -
    if regime == 'laminar':  # Holman1972
        Sh = 3.66 + 0.2672 / (xi + 0.10079 * xi**(1/3))  # -
    elif regime == 'turbulent':  # Friedlander1977
        Sh = 0.0118 * Re_f**(7/8) * Sc**(1/3)  # -
    return Sh


def eta_tube_diff(particle_diameter: float,
                  ambient_pressure: float,
                  ambient_temperature: float,
                  tube_length: float,
                  tube_diameter: float,
                  volume_flow: float,
                  relative_humidity: float,
                  regime: str) -> float:
    """
    Diffusional deposition in sample lines for laminar or turbulent flow.
    Taken from [1], p.90.

    Parameters
    ----------
    particle_diameter : float
        Particle diameter in m.
    ambient_pressure : float
        Ambient pressure in Pa.
    ambient_temperature : float
        Ambient temperature in K.
    tube_length : float
        Length of tube in m.
    tube_diameter : float
        Diameter of tube in m.
    volume_flow : float
        Volume flow in l/min.
    relative_humidity : float
        Relative humidity, range = (0 ... 1), dimensionless.
    regime : str
        Flow regime for the setup.
        Needs to be either 'laminar'
        or 'turbulent'. The flow regime
        can be checked by calculated the
        Reynolds number (reynolds_number()).

    Returns
    -------
    float
        Diffusional deposition.

    References
    ----------
    .. [1] Kulkarni, P. (2011). Aerosol measurement : principles, techniques, and applications.
           Ed. by P. Kulkarni, P. A. Baron, and K. Willeke. Hoboken, N.J: Wiley. 
           ISBN: 9780470387412.
    """
    d_p = particle_diameter  # m
    p = ambient_pressure  # Pa
    T = ambient_temperature  # K
    L = tube_length  # m
    Q = volume_flow  # l / min
    d = tube_diameter  # m
    RH = relative_humidity  # -
    xi = xi_func(particle_diameter=d_p,
                 ambient_pressure=p,
                 ambient_temperature=T,
                 tube_length=L,
                 volume_flow=Q)  # -
    Sh = sherwood(particle_diameter=d_p,
                  ambient_pressure=p,
                  ambient_temperature=T,
                  tube_length=L,
                  tube_diameter=d,
                  volume_flow=Q,
                  relative_humidity=RH,
                  regime=regime)  # -
    eta_tube_diff = np.exp(- xi * Sh)
    return eta_tube_diff


def turbulent_inertial_deposition_velocity(particle_diameter: float,
                                           ambient_pressure: float,
                                           ambient_temperature: float,
                                           volume_flow: float,
                                           tube_diameter: float,
                                           relative_humidity: float,
                                           density: float) -> float:
    """
    Turbulent inertial deposition velocity in sample lines for turbulent flow.
    Taken from [1], p. 92.

    Parameters
    ----------
    particle_diameter : float
        Particle diameter in m.
    ambient_pressure : float
        Ambient pressure in Pa.
    ambient_temperature : float
        Ambient temperature in K.
    volume_flow : float
        Volume flow in l/min.
    tube_diameter : float
        Diameter of tube in m.
    relative_humidity : float
        Relative humidity, range = (0 ... 1), dimensionless.
    density : float
        Aerosol particle density in kg / m3.
        Unit density is 1000 kg / m3.

    Returns
    -------
    float
        Turbulent inertial deposition velocity.
        Used in eta_tube_turb_inertial().

    References
    ----------
    .. [1] Kulkarni, P. (2011). Aerosol measurement : principles, techniques, and applications.
           Ed. by P. Kulkarni, P. A. Baron, and K. Willeke. Hoboken, N.J: Wiley.
           ISBN: 9780470387412.
    """
    d_p = particle_diameter  # m
    T = ambient_temperature  # K
    Q = volume_flow  # l / min
    d = tube_diameter  # m
    rho_p = density  # kg / m3
    p = ambient_pressure  # Pa
    RH = relative_humidity  # -
    rho = density_of_air(ambient_pressure=p,
                         ambient_temperature=T,
                         relative_humidity=RH)
    Re_f = reynolds_number(flow=Q,
                           pipe_diameter=d,
                           characteristic_diameter=d,
                           ambient_temperature=T,
                           density=rho)  # -
    Stk = stokes_number(particle_diameter=d_p,
                        particle_density=rho_p,
                        pore_diameter=d,
                        flow=Q,
                        filter_diameter=d,
                        ambient_pressure=p,
                        ambient_temperature=T)  # -
    U = face_velocity(Q, d)  # m / s
    tau_plus = 0.0395 * Stk * Re_f**(3/4)  # -
    if tau_plus <= 12.9:  # Lee & Gieseke 1994, Kulkarni2011, p.92
        V_plus = 6e-4 * tau_plus**2 + 2e-8 * Re_f
    elif tau_plus > 12.9:  # Lie & Agarwal 1974, Kulkarni2011, eq.6-65, p.92
        V_plus = 0.1

    V_t = V_plus * U / Re_f**(1/8) / 5.03  # -

    return V_t


def eta_tube_turb_inertial(particle_diameter: float,
                           ambient_pressure: float,
                           ambient_temperature: float,
                           tube_length: float,
                           volume_flow: float,
                           tube_diameter: float,
                           density: float,
                           relative_humidity: float) -> float:
    """
    Turbulent inertial deposition in sample lines for turbulent flow.
    Taken from [1], p. 92.

    Parameters
    ----------
    particle_diameter : float
        Particle diameter in m.
    ambient_pressure : float
        Ambient pressure in Pa.
    ambient_temperature : float
        Ambient temperature in K.
    tube_length : float
        Length of tube in m.
    volume_flow : float
        Volume flow in l/min.
    tube_diameter : float
        Diameter of tube in m.
    density : float
        Aerosol particle density in kg / m3.
        Unit density is 1000 kg / m3.

    Returns
    -------
    float
        Transport efficiency in a tube with turbulent inertial deposition of particles.

    References
    ----------
    .. [1] Kulkarni, P. (2011). Aerosol measurement : principles, techniques, and applications.
           Ed. by P. Kulkarni, P. A. Baron, and K. Willeke. Hoboken, N.J: Wiley.
           ISBN: 9780470387412.
    """
    d_p = particle_diameter  # m
    p = ambient_pressure  # Pa
    T = ambient_temperature  # K
    L = tube_length  # m
    Q = volume_flow  # l / min
    d = tube_diameter  # m
    rho_p = density  # kg / m3
    RH = relative_humidity  # -
    V_t = turbulent_inertial_deposition_velocity(particle_diameter=d_p,
                                                 ambient_pressure=p,
                                                 ambient_temperature=T,
                                                 volume_flow=Q,
                                                 tube_diameter=d,
                                                 relative_humidity=RH,
                                                 density=rho_p)
    Q = 1e-3 / 60 * Q  # flow, l / min -> m3 / s
    eta_tube_turb_intertial = np.exp(- np.pi * d * L * V_t / Q)
    return eta_tube_turb_intertial


def inertial_deposition_bend(particle_diameter: float,
                             ambient_pressure: float,
                             ambient_temperature: float,
                             volume_flow: float,
                             tube_diameter: float,
                             phi: float,
                             density: float,
                             regime: str) -> float:
    """
    Transport efficiency of particles through a bend in laminar or turbulent flow.
    Taken from [1], p.94

    Parameters
    ----------
    particle_diameter : float
        Particle diameter in m.
    ambient_pressure : float
        Ambient pressure in Pa.
    ambient_temperature : float
        Ambient temperature in K.
    volume_flow : float
        Volume flow in l/min.
    tube_diameter : float
        Diameter of tube in m.
    phi : float
        Angle of bend in °.
    density : float
        Aerosol particle density in kg / m3.
        Unit density is 1000 kg / m3.
    regime : str
        Flow regime for the setup.
        Needs to be either 'laminar'
        or 'turbulent'. The flow regime
        can be checked by calculated the
        Reynolds number (reynolds_number()).

    Returns
    -------
    float
        Transport efficiency of particles through a bend in laminar or turbulent flow.

    References
    ----------
    .. [1] Kulkarni, P. (2011). Aerosol measurement : principles, techniques, and applications.
           Ed. by P. Kulkarni, P. A. Baron, and K. Willeke. Hoboken, N.J: Wiley.
           ISBN: 9780470387412.
    """
    d_p = particle_diameter  # m
    p = ambient_pressure  # Pa
    T = ambient_temperature  # K
    Q = volume_flow  # l / min
    d = tube_diameter  # m
    rho_p = density
    Stk = stokes_number(particle_diameter=d_p,
                        particle_density=rho_p,
                        pore_diameter=d,
                        flow=Q,
                        filter_diameter=d,
                        ambient_pressure=p,
                        ambient_temperature=T)
    if regime == 'laminar':
        eta_bend_inert = (1 + (Stk / 0.171)**(0.452 * Stk / 0.171 + 2.242))**(-2 / np.pi * phi)
    elif regime == 'turbulent':
        eta_bend_inert = np.exp(- 2.823 * Stk * phi)
    return eta_bend_inert


def total_transport_efficiency(particle_diameter: float,
                               ambient_pressure: float,
                               ambient_temperature: float,
                               tube_length: float,
                               tube_diameter: float,
                               volume_flow: float,
                               relative_humidity: float,
                               density: float,
                               theta: float,
                               phi: float,
                               regime: str,
                               number_of_bends: int,
                               verbose: bool = True) -> float:
    """
    Wrapper function to combine multiple different mechanism in regard
    to the transport efficiency of aerosol particles inside tubes.
    This function considers four different loss mechanisms:
        1) see eta_tube_grav().
        2) eta_tube_diff().
        3) eta_tube_turb_inertial().
        4) inertial_deposition_bend().

    Parameters
    ----------
    particle_diameter : float
        Particle diameter in m.
    ambient_pressure : float
        Ambient pressure in Pa.
    ambient_temperature : float
        Ambient temperature in K.
    tube_length : float
        Length of tube in m.
    tube_diameter : float
        Diameter of tube in m.
    volume_flow : float
        Volume flow in l/min.
    relative_humidity : float
        Relative humidity, range = (0 ... 1), dimensionless.
    density : float
        Aerosol particle density in kg / m3.
        Unit density is 1000 kg / m3.
    theta : float
        Angle of inclination.
        0 for a horizontal tube.
        90 for a vertical tube.
    phi : float
        Angle of bend in °.
    regime : str
        Flow regime for the setup.
        Needs to be either 'laminar'
        or 'turbulent'. The flow regime
        can be checked by calculated the
        Reynolds number (reynolds_number()).
    number_of_bends : int
        Number of bends in the system.
    verbose : bool, optional
        Only applicable for float inputs.
        Return information to the individual loss functions.
        The default is True.

    Returns
    -------
    float
        Total transport efficiency for particles of
        the given size during the given conditions.

    """
    d_p = particle_diameter  # m
    p = ambient_pressure  # Pa
    T = ambient_temperature  # K
    L = tube_length  # m
    d = tube_diameter  # m
    Q = volume_flow  # l / min
    RH = relative_humidity  # -
    rho_p = density  # kg / m3

    eta_grav = eta_tube_grav(particle_diameter=d_p,
                             ambient_pressure=p,
                             ambient_temperature=T,
                             tube_length=L,
                             tube_diameter=d,
                             volume_flow=Q,
                             particle_density=rho_p,
                             theta=theta,
                             regime=regime)
    eta_diff = eta_tube_diff(particle_diameter=d_p,
                             ambient_pressure=p,
                             ambient_temperature=T,
                             tube_length=L,
                             tube_diameter=d,
                             volume_flow=Q,
                             relative_humidity=RH,
                             regime=regime)
    if regime == 'laminar':
        eta_inert = d_p / d_p
    elif regime == 'turbulent':
        eta_inert = [eta_tube_turb_inertial(particle_diameter=x,
                                            ambient_pressure=p,
                                            ambient_temperature=T,
                                            tube_length=L,
                                            volume_flow=Q,
                                            tube_diameter=d,
                                            density=rho_p,
                                            relative_humidity=RH) for x in d_p]

    if number_of_bends == 0:
        eta_bend = 1
    else:
        eta_bend = inertial_deposition_bend(particle_diameter=d_p,
                                            ambient_pressure=p,
                                            ambient_temperature=T,
                                            volume_flow=Q,
                                            tube_diameter=d,
                                            phi=phi,
                                            density=rho_p,
                                            regime=regime)

    eta_total = eta_grav * eta_inert * eta_diff * eta_bend**(number_of_bends)

    if verbose:
        print('For your given configuration the transport efficiency was calculated:\n'
              f'The gravitational settling efficieny is {eta_grav:.4f}.\n'
              'The inertial settling efficiency (only for turbulent flow regime) '
              f'is {eta_inert:.4f}.\n'
              f'The diffusional settling efficiency is {eta_diff:.4f}.\n'
              f'The efficiency due to bends in your system is {eta_bend:.4f}.\n'
              f'In total this results in a total efficiency of {eta_total:.4f}.')

    return eta_total


def find_half_collection_efficiency(efficiency: npt.ArrayLike) -> tuple[int, int]:
    """
    Helper function to estimate lower and upper 0.5 transmission efficiency.
    Will not work correctly if you do not have a clear maximum in your efficiency curve.

    Parameters
    ----------
    efficiency : npt.ArrayLike[float]
        Transport efficiency as given by the various functions given here.

    Returns
    -------
    tuple[int, int]
        Index of the 0.5 transmission efficiency, for lower and upper bound.
    """
    new_array = np.copy(efficiency)
    max_value = new_array.argmax()
    lower_array = new_array[:max_value]
    upper_array = new_array[max_value:]
    first_half_point = np.abs(lower_array - 0.5).argmin()
    second_half_point = np.abs(upper_array - 0.5).argmin()
    return (first_half_point, second_half_point+max_value)
