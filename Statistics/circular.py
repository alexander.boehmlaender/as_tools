# -*- coding: utf-8 -*-
"""
Created on Tue Aug 31 15:26:30 2021

@author: st5536

Functions to calculate correlations between linear
and circular timeseries or two circular timeseries.
"""

import numpy as np
import numpy.typing as npt
import typing
from scipy.stats import spearmanr


def lin_circ_corr(linear_data: npt.ArrayLike,
                  circular_data: npt.ArrayLike) -> typing.Tuple[float, float]:
    """
    Calculate spearman correlation between one linear timeseries and one circular timeseries.

    Parameters
    ----------
    linear_data : npt.ArrayLike
        Linear timeseries.
    circular_data : npt.ArrayLike
        Circular timeseries.

    Returns
    -------
    tuple[float, float]
        Spearman correlation coefficient r with corresponding p value.

    """
    circular_data = circular_data / 360 * 2 * np.pi

    r_sx, p_sx = spearmanr(linear_data, np.sin(circular_data))
    r_cx, p_cx = spearmanr(linear_data, np.cos(circular_data))
    r_cs, p_cs = spearmanr(np.sin(circular_data),
                           np.cos(circular_data))
    r_ = np.sqrt((r_cx**2 + r_sx**2 - 2 * r_cx * r_sx * r_cs) / (1 - r_cs**2))
    p_ = np.sqrt((p_cx**2 + p_sx**2 - 2 * p_cx * p_sx * p_cs) / (1 - p_cs**2))
    return r_, p_


def circ_circ_corr(circular_data1: npt.ArrayLike, circular_data2: npt.ArrayLike) -> float:
    """
    Calculate correlation between two circular timeseries.

    Parameters
    ----------
    circular_data1 : npt.ArrayLike
        First circular timeseries (given in degrees).
    circular_data2 : npt.ArrayLike
        Second circular timeseries (given in degrees).

    Returns
    -------
    float
        Correlation coefficient r.

    """
    data1 = circular_data1 / 360 * 2 * np.pi
    data2 = circular_data2 / 360 * 2 * np.pi

    sinus_sum_data1 = sum(np.sin(data1))
    cosinus_sum_data1 = sum(np.cos(data1))

    sinus_sum_data2 = sum(np.sin(data2))
    cosinus_sum_data2 = sum(np.cos(data2))

    sample_mean_direction_data1 = np.arctan2(sinus_sum_data1, cosinus_sum_data1)
    sample_mean_direction_data2 = np.arctan2(sinus_sum_data2, cosinus_sum_data2)

    r_cc = (sum((np.sin(data1 - sample_mean_direction_data1))
                * (np.sin(data2 - sample_mean_direction_data2)))
            / np.sqrt(sum(np.sin(data1 - sample_mean_direction_data1)**2)
                      * sum(np.sin(data2 - sample_mean_direction_data2)**2)))
    return r_cc
