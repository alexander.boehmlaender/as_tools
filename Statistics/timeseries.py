# -*- coding: utf-8 -*-
"""
Created on Tue Aug 10 14:50:39 2021

@author: st5536

Functions for analysis of time series. To make life easier.
"""

import pandas as pd


def average_data_over_time_slot(df: pd.DataFrame, start_time: list,
                                stop_time: list, mean_time: list) -> pd.DataFrame:
    """
    Average a timeseries over a multiple time slots defined by start, stop and mean time lists.
    Originally done to average data over INSEKT filter collection times.
    Hallo?!

    Parameters
    ----------
    df : pd.DataFrame
        Original timeseries given as a Pandas DataFrame with a DatetimeIndex.
    start_time : list
        List of start times.
    stop_time : list
        List of stop times.
    mean_time : list
        List of mean times.

    Raises
    ------
    Exception
        - if start_time and stop_time have different lengths
        - if stop_time and mean_time have different lengths
        - if df is empty
        - if df.index is not monotonic increasing

    Returns
    -------
    df_averaged : pd.DataFrame
        Averaged timeseries with a DatetimeIndex corresponding to the mean_time.

    """
    if len(start_time) != len(stop_time):
        raise Exception('start_time and stop_time '
                        'have to have the same length')
    if len(stop_time) != len(mean_time):
        raise Exception('stop_time and mean_time '
                        'have to have the same length')
    if df.empty:
        raise Exception('Your dataframe is empty.')
    if not df.index.is_monotonic_increasing:
        raise Exception('Your index is not monotonically increasing.')
    df_dict = {}
    for col in df.columns:
        try:
            df[col] = df[col].astype(float)
        except Exception as e:
            print('Exception:', e)
        if df[col].dtype != (float or int):
            df.drop(col, axis=1)
            print(f'Column {col} dropped, because values are not float or int.')
            continue
        lst_mean = []
        for idx in range(len(start_time)):
            lst_mean.append(df.loc[start_time[idx]:stop_time[idx], col].mean())
        df_dict[col] = pd.Series(lst_mean)
    df_averaged = pd.DataFrame(df_dict)

    df_averaged.index = mean_time
    df_averaged.index.name = 'DateTime'

    return df_averaged
