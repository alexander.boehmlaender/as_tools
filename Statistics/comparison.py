#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Nov 14 13:19:58 2024

@author: alex
## class shamelessly copied from py_aida project by Tobias Schorr
## adding some additional parameters
"""

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt


class SeriesComparator:
    """Compare two pd.Series with respect to specific metrics."""

    def __init__(self, series_a, series_b):
        self._series_a_original = series_a
        self._series_b_original = series_b
        self.series_a = self._series_a_original.loc[self.create_joined_DatetimeIndex(
            i1=self._series_a_original.index,
            i2=self._series_b_original.index)]
        self.series_b = self._series_b_original.loc[self.create_joined_DatetimeIndex(
            i1=self._series_a_original.index,
            i2=self._series_b_original.index)]
        self.mean_error = self.mean_error_f()
        self.mean_absolute_error = self.mean_absolute_error_f()
        self.mean_normalized_bias = self.mean_normalized_bias_f()
        self.mean_absolute_relative_error = self.mean_absolute_relative_error_f()
        self.fractional_bias = self.fractional_bias_f()
        self.fractional_absolute_error = self.fractional_absolute_error_f()
        self.root_mean_square = self.root_mean_square_f()

    def create_joined_DatetimeIndex(self,
                                    i1: pd.DatetimeIndex,
                                    i2: pd.DatetimeIndex) -> pd.DatetimeIndex:
        """Get the intersection of two DatetimeIndex objects."""

        if not (isinstance(i1, pd.DatetimeIndex) and isinstance(i2, pd.DatetimeIndex)):
            raise TypeError(
                "Both arguments have or be of type pd.Datetimeindex. "
                f"You passed {type(i1)} and {type(i2)}."
            )

        joined_index = i1.intersection(i2)

        for i in [i1, i2]:
            if (len(i) != len(joined_index)) and (
                len(joined_index) < 0.9 * len(i) or len(joined_index) > 1.1 * len(i)
            ):
                print(
                    "Length of joined DatetimeIndex varies by more than 10% from the input array."
                )

        return joined_index

    def get_joined_DatetimeIndex(self):
        return self.create_joined_DatetimeIndex(i1=self.series_a.index,
                                                i2=self.series_b.index)

    def mean_error_f(self):
        mean_error = 1 / len(self.series_a) * (self.series_a - self.series_b).sum()
        return mean_error

    def mean_absolute_error_f(self):
        mean_absolute_error = 1 / len(self.series_a) * abs(self.series_a - self.series_b).sum()
        return mean_absolute_error

    def mean_normalized_bias_f(self):
        mean_normalized_bias = (1 / len(self.series_a)
                                * ((self.series_a - self.series_b) / self.series_a).sum()
                                )
        return mean_normalized_bias

    def mean_absolute_relative_error_f(self):
        mean_absolute_relative_error = (
            1 / len(self.series_a) * (abs(self.series_a - self.series_b)
                                      / abs(self.series_a)).sum()
            )
        return mean_absolute_relative_error

    def fractional_bias_f(self):
        fractional_bias = (
            1 / len(self.series_a) * (2 * (self.series_a - self.series_b)
                                      / (self.series_a + self.series_b)).sum()
            )
        return fractional_bias

    def fractional_absolute_error_f(self):
        fractional_absolute_error = (
            1 / len(self.series_a) * (2 * abs(self.series_a - self.series_b)
                                      / (abs(self.series_a) + abs(self.series_b))).sum()
            )
        return fractional_absolute_error

    def root_mean_square_f(self):
        root_mean_square = np.sqrt(1 / len(self.series_a)
                                   * ((self.series_a - self.series_b) ** 2).sum())
        return root_mean_square

    def info(self):
        """Parameters."""
        info_str = f"""
        MBE = {self.mean_error:.2f}\n
        MAE = {self.mean_absolute_error:.2f}\n
        MNB = {self.mean_normalized_bias:.2f}\n
        MARE = {self.mean_absolute_relative_error:.2f}\n
        FB = {self.fractional_bias:.2f}\n
        FAE = {self.fractional_absolute_error:.2f}\n
        RMSE = {self.root_mean_square:.2f}
        """
        return info_str

    def pprint(self):
        """Print parameters."""
        print("Statistics for given series (A vs. B).")
        print("Parameters are provided with a range and an 'ideal' value, as in")
        print("parameter (range, ideal_value): parameter_value")
        print("if parameter_value is inf, normalizing factor is likely 0\n")

        print("Mean bias error (-inf(B larger) to inf(A larger), 0):",
              self.mean_error)
        print("Mean absolute error (0 to inf, 0): ",
              self.mean_absolute_error)
        print("Mean normalized bias (-inf(B larger) to inf(A larger), 0):",
              self.mean_normalized_bias)
        print("Mean absolute relative error (0 to inf, 0)",
              self.mean_absolute_relative_error)
        print("Fractional bias (-2(B larger) to 2(A larger), 0):",
              self.fractional_bias)
        print("Fractional absolute error (0 to 2, 0):",
              self.fractional_absolute_error)
        print("Root mean square error (0 to inf, 0):",
              self.root_mean_square)


class SeriesComparatorPlot(SeriesComparator):
    def __init__(self, series_a, series_b):
        super().__init__(series_a, series_b)

    def plot(self, fractions, scale, **plot_kw_args):
        fig = plt.gcf()
        ax = plt.gca()
        ax.plot(self.series_a,
                self.series_b,
                marker='o',
                markersize=fig.dpi / 72.27,
                linestyle='',
                zorder=1,
                **plot_kw_args)

        ax.set_xscale(scale)
        ax.set_yscale(scale)

        lims = [
            np.min([ax.get_xlim(), ax.get_ylim()]),  # min of both axes
            np.max([ax.get_xlim(), ax.get_ylim()]),  # max of both axes
        ]

        # now plot both limits against eachother
        ax.plot(lims, lims, 'k-', zorder=2, label='factor 1')
        x = np.linspace(min(lims), max(lims), 2)
        for i, fraction in enumerate(fractions):
            _amount = self.fraction_amount(fraction)
            ax.plot(x, fraction * x,
                    linestyle=['--', '-.', ':', '--', '-.', ':'][i],
                    marker='', color=f'C{i+1}', zorder=2,
                    label=f'factor {fraction} ({_amount * 100:.2f} %)')
            ax.plot(x, 1/fraction * x,
                    linestyle=['--', '-.', ':', '--', '-.', ':'][i],
                    marker='', color=f'C{i+1}', zorder=2)

        ax.set_aspect('equal')
        ax.set_xlim(lims)
        ax.set_ylim(lims)
        ax.text(0.99, 1, self.info(),
                linespacing=0.5,
                horizontalalignment='left',
                verticalalignment='top',
                transform=ax.transAxes,
                bbox=dict(facecolor='white', edgecolor='grey', alpha=0.8,
                          boxstyle='round,pad=0.1,rounding_size=0.4'))
        ax.legend(bbox_to_anchor=(1, 0.5))

    def fraction_amount(self, fraction):
        amount = len(self.series_a.loc[(self.series_a >= (fraction * self.series_b))
                                       | (self.series_a < (1 / fraction * self.series_b))]) / len(self.series_a)
        return 1 - amount
