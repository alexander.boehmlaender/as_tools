#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Nov 24 00:07:14 2024

@author: alex
"""

import numpy as np
import pandas as pd
import math
import matplotlib.pyplot as plt
import os
from pathlib import Path
os.chdir(Path(__file__).parent)
from Calculations import size_distribution as sd

plot_path = Path(__file__).parent.joinpath('test_results/Calculations')
plot_path.mkdir(exist_ok=True, parents=True)

# Test size distributions and create suitable plots.
# Generate three modes of a size distribution in urban areas.

particle_diameter = np.geomspace(1e-3, 10, 500)  # µm

number_size_distribution_nucleation = sd.lognormal_number_distribution_log_scale(
    number_concentration=7100,
    standard_deviation=np.exp(0.232),
    median_particle_diameter=0.0117,
    particle_diameter=particle_diameter)

number_size_distribution_aitken = sd.lognormal_number_distribution_log_scale(
    number_concentration=6320,
    standard_deviation=np.exp(0.250),
    median_particle_diameter=0.0373,
    particle_diameter=particle_diameter)

number_size_distribution_coarse = sd.lognormal_number_distribution_log_scale(
    number_concentration=960,
    standard_deviation=np.exp(0.204),
    median_particle_diameter=0.151,
    particle_diameter=particle_diameter)

# test for the number of particles, the area under dNdlogdp should be the total
# number concentration
dlogdp = np.diff(np.log10(particle_diameter)).mean()
number_concentration_nucleation = (number_size_distribution_nucleation
                                   * dlogdp).sum()
assert math.isclose(7100, number_concentration_nucleation)
number_concentration_aitken = (number_size_distribution_aitken
                               * dlogdp).sum()
assert math.isclose(6320, number_concentration_aitken)
number_concentration_coarse = (number_size_distribution_coarse
                               * dlogdp).sum()
assert math.isclose(960, number_concentration_coarse)

# %%
fig, ax = plt.subplots()
ax.plot(particle_diameter,
        number_size_distribution_nucleation, label='Nucleation')
ax.plot(particle_diameter,
        number_size_distribution_aitken, label='Aitken')
ax.plot(particle_diameter,
        number_size_distribution_coarse, label='Coarse')
ax.plot(particle_diameter,
        number_size_distribution_aitken
        + number_size_distribution_nucleation
        + number_size_distribution_coarse,
        label='Total')

ax.set_xlabel(r'$d_\mathrm{p}$ / µm')
ax.set_ylabel(r'd$N$/dlog$d_\mathrm{p}$ / cm${-3}$')

ax.set_xscale('log')
ax.legend()
fig.savefig(plot_path.joinpath('test_sd.png'))

# %% Testing calculations of surface and volume distributions
total_sd = (number_size_distribution_aitken
            + number_size_distribution_nucleation
            + number_size_distribution_coarse)
total_sd = pd.DataFrame(total_sd, index=particle_diameter).T

fig, (ax, ax2, ax3) = plt.subplots(3, 1, sharex=True)
ax.plot(particle_diameter,
        total_sd.iloc[0, :],
        label='Number')
ax2.plot(particle_diameter,
         sd.surface_distribution(total_sd).iloc[0, :],
         label='Surface')
ax3.plot(particle_diameter,
         sd.volume_distribution(total_sd).iloc[0, :],
         label='Volume')

for name, axis in zip(('N', 'S', 'V'), (ax, ax2, ax3)):
    axis.set_ylabel(fr'd${name}$/dlog$d_\mathrm{{p}}$ / cm${-3}$')
    axis.set_xscale('log')
    axis.legend()
ax3.set_xlabel(r'$d_\mathrm{p}$ / µm')

fig.savefig(plot_path.joinpath('test_sd_NSV.png'))
